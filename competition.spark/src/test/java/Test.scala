import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions._

/**
 * Author: xl
 * Date: 2022/1/16 15:39
 * program: com.xl.competition
 * Email: 2199396150@qq.com
 * Description: ${description}
 */
object Test {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "xxl")
    Logger.getLogger("org").setLevel(Level.ERROR)
    val spark: SparkSession = SparkSession.builder().appName(this.getClass.getName).master("local[*]")
      .getOrCreate()
    import spark.implicits._
    val frame: DataFrame = spark.read.option("header", "true").csv("file:\\E:\\IDEA_BigData\\com.xl.competition\\competition.spark\\src\\main\\resources\\test.csv")
    frame.show()
    val spec: WindowSpec = Window.partitionBy(col("product_code")).orderBy(col("event_date"))
    frame.select(
      col("product_code"),
      col("event_date"),
      sum(col("duration")) over (spec) as ("result")
    )
      .show()


    spark.stop()
  }
}
