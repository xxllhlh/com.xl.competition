--根据 dwd 层表统计每个省份、每个地区、每个月下单的数量和下单的总金额
describe dwd.fact_order_info;
--provinceid int 省份表主键
--provincename text 省份名称
--regionid int 地区表主键
--regionname text 地区名称
--totalconsumption double 订单总金额 当月订单总金额
--totalorder int 订单总数 当月订单总数
--year int 年 订单产生的年
--month int 月 订单产生的月
create table provinceeverymonth
(
    provinceid       int,
    provincename     varchar(16),
    regionid         int,
    regionname       varchar(16),
    totalconsumption double,
    totalorder       int,
    year             int,
    month            int
);


with area as (
    select pro.id          provinceid,
           pro.name        provincename,
           reg.id          regionid,
           reg.region_name regionname
    from dim.dim_base_province pro
             join dim.dim_base_region reg
                  on pro.region_id = reg.id
),
     ori as (
         select id,
                total_amount,
                province_id,
                create_time
         from dwd.fact_order_info
     )
select provinceid,
       provincename,
       regionid,
       regionname,
       cast(sum(total_amount) as double) totalconsumption,
       count(id),
       --20231116
       substr(create_time, 0, 4)         year,
       substr(create_time, 5)            month
from ori
         join area
              on province_id = provinceid
group by provinceid, provincename, regionid, regionname, substr(create_time, 0, 4), substr(create_time, 5)
;

-- provinceid int 省份表主键
-- provincename text 省份名称
-- provinceavgconsumption double 该省平均订单金额
-- allprovinceavgconsumption double 所有省平均订单金额
-- comparison text 比较结果 该省平均订单金额和所有省平均订单金额比较 结果，值为：高/低/相同

create table provinceavgcmp
(
    provinceid                int,
    provincename              varchar(16),
    provinceavgconsumption    double,
    allprovinceavgconsumption double,
    comparison                varchar(16)
);

--请根据 dwd 层表计算出 2023 年 11 月每个省份的平均订单金额和所有省份平均订单金额相比较结果（“高/低/相同”）
select provinceid,
       provincename,
       provinceavgconsumption,
       allprovinceavgconsumption,
       case
           when provinceavgconsumption > allprovinceavgconsumption then '高'
           when provinceavgconsumption < allprovinceavgconsumption then '低'
           else '相同' end comparison
from (
         select province_id       provinceid,
                pro.name          provincename,
                avg(total_amount) provinceavgconsumption,
                (
                    select sum(total_amount) / count(*)
                    from fact_order_info
                )                 allprovinceavgconsumption
         from dwd.fact_order_info foi
                  join dim.dim_base_province pro
                       on province_id = pro.id
         where substr(foi.create_time, 0, 6) = '202311'
         group by province_id, pro.name
     ) t1
;

-- userid int 客户主键
-- username text 客户名称
-- day text 日 记录下单日的时间，格式为yyyyMMdd_yyyyMMdd 例如： 20220101_20220102
-- totalconsumption double 订单总金额 连续两天的订单总金额
-- totalorder int 订单总数 连续两天的订单总数

create table usercontinueorder
(
    userid           int,
    username         varchar(16),
    day              varchar(32),
    totalconsumption double,
    totalorder       int
)
;

--根据 dwd 层表统计在两天内连续下单并且下单金额保持增长的用户
select userid,
       username,
       concat(befor, "_", create_time) day,
       all_amount + before_amount      totalconsumption,
       totalorder
from (
         select user_id                                                                                       userid,
                username,
                create_time,
                lag(create_time, 1, "") over (partition by user_id order by create_time)                      befor,
                all_amount,
                lag(all_amount, 1, 99999999)
                    over (partition by user_id order by create_time)                                          before_amount,
                sum(totalorder)
                    over (partition by user_id order by create_time ROWS BETWEEN 1 PRECEDING and CURRENT ROW) totalorder
         from (
                  select t3.user_id,
                         create_time,
                         username,
                         sum(total_amount) all_amount,
                         count(id)         totalorder
                  from dwd.fact_order_info foi
                           join (
                      SELECT t2.user_id,
                             username,
                             count(1) as login_times
                      FROM (
                               SELECT t1.user_id,
                                      t1.name                      username,
                                      t1.create_time,
                                      date_sub(create_time, rn) as diff_date
                               FROM (
                                        SELECT user_id,
                                               name,
                                               from_unixtime(unix_timestamp(foi1.create_time, 'yyyyMMdd'),
                                                             'yyyy-MM-dd')                                           create_time,
                                               row_number() over (partition by user_id order by foi1.create_time) as rn
                                        FROM dwd.fact_order_info foi1
                                                 join dim.dim_user_info
                                                      on user_id = dim_user_info.id
                                        group by user_id, name, foi1.create_time
                                    ) t1
                           ) t2
                      group by t2.user_id, username, t2.diff_date
                      having login_times >= 2
                  ) t3
                                on foi.user_id = t3.user_id
                  group by t3.user_id, t3.username, create_time
                  order by user_id
              ) t4
     ) t5
where all_amount > before_amount
;


select user_id,
       sum(total_amount),
       create_time,
       lag(create_time, 1, "") over (partition by user_id order by create_time) befor
from dwd.fact_order_info f
group by user_id, create_time
;



select id, user_id, total_amount, create_time
from fact_order_info
order by user_id, create_time



select cast(t1.id as int)         provinceid,
       cast(t1.name as string)    provincename,
       cast(t2.pavg as double)    provinceavgconsumption,
       cast(t2.allpavg as double) allprovinceavgconsumption,
       CASE
           WHEN t2.pavg > t2.allpavg THEN '高'
           WHEN t2.pavg < t2.allpavg THEN '低'
           ELSE '相同'
           END                    comparison
from dim.dim_base_province t1
         join (
    select tmp.province_id       id,
           AVG(tmp.total_amount) pavg,
           (
               select AVG(total_amount)
               from dwd.fact_order_info
               where YEAR(create_time) = 2023
                 and MONTH(create_time) = 4
           )                     allpavg
    from dwd.fact_order_info tmp
    where substr(tmp.create_time, 0, 6) = '202311'
    group by tmp.province_id
) t2