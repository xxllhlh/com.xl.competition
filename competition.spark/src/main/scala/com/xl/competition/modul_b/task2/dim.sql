create database dim;

use dim;

create table dim.dim_user_info as
select *
from ods.user_info
where id < 1;


CREATE TABLE `dim_user_info`
(
    `id`            bigint,
    `login_name`    string,
    `nick_name`     string,
    `passwd`        string,
    `name`          string,
    `phone_num`     string,
    `email`         string,
    `head_img`      string,
    `user_level`    string,
    `birthday`      date,
    `gender`        string,
    `create_time`   timestamp,
    `operate_time`  timestamp,
    `status`        string,
    dwd_insert_user string,
    dwd_insert_time string,
    dwd_modify_user string,
    dwd_modify_time string

) partitioned by (etl_date string)
    STORED AS INPUTFORMAT
        'org.apache.hadoop.mapred.TextInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dim.db/dim_user_info';

insert into table dim.dim_user_info partition (etl_date = '20231115')
select id,
       login_name,
       nick_name,
       passwd,
       name,
       phone_num,
       email,
       head_img,
       user_level,
       birthday,
       gender,
       create_time,
       operate_time,
       status,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.user_info
where etl_date = '20231115';


with dsku as (
    select id,
           spu_id,
           price,
           sku_name,
           sku_desc,
           weight,
           tm_id,
           category3_id,
           sku_default_img,
           is_sale,
           create_time,
           dwd_insert_user,
           dwd_insert_time,
           dwd_modify_user,
           dwd_modify_time
    from dim.dim_sku_info
),
     osku as (
         select id,
                spu_id,
                price,
                sku_name,
                sku_desc,
                weight,
                tm_id,
                category3_id,
                sku_default_img,
                is_sale,
                create_time,
                'user1'                                                dwd_insert_user,
                'user1'                                                dwd_modify_user,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
         from ods.sku_info
         where etl_date = '20231116'
     )
select if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.id, dsku.id) as id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.spu_id,
          dsku.spu_id)                                                                                          as spu_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.price,
          dsku.price)                                                                                           as price,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_name,
          dsku.sku_name)                                                                                        as sku_name,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_desc,
          dsku.sku_desc)                                                                                        as sku_desc,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.weight,
          dsku.weight)                                                                                          as weight,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.tm_id,
          dsku.tm_id)                                                                                           as tm_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.category3_id,
          dsku.category3_id)                                                                                    as category3_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_default_img,
          dsku.sku_default_img)                                                                                 as sku_default_img,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.is_sale,
          dsku.is_sale)                                                                                         as is_sale,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.create_time,
          dsku.create_time)                                                                                     as create_time,
       'user1'                                                                                                  as dwd_insert_user,
       nvl(dsku.dwd_insert_time, substr(current_timestamp(), 1, 19))                                            as dwd_insert_time,
       'user1'                                                                                                  as dwd_modify_user,
       substr(current_timestamp(), 1, 19)                                                                       as dwd_modify_time
from osku
         left join dsku
                   on osku.id = dsku.id
;
select unix_timestamp('2023-11-15 16:17:37.000000000');
select from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss');
select substr(current_timestamp(), 1, 19);

select dwd_insert_time
from dim.dim_user_info
where etl_date = '20231113';

select current_timestamp()
;

CREATE TABLE dim.dim_sku_info
(
    `id`              bigint,
    `spu_id`          bigint,
    `price`           decimal(10, 0),
    `sku_name`        string,
    `sku_desc`        string,
    `weight`          decimal(10, 2),
    `tm_id`           bigint,
    `category3_id`    bigint,
    `sku_default_img` string,
    `is_sale`         int,
    `create_time`     timestamp,
    dwd_insert_user   string,
    dwd_insert_time   string,
    dwd_modify_user   string,
    dwd_modify_time   string
)
    PARTITIONED BY (
        `etl_date` string)
    ROW FORMAT SERDE
        'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
        'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dim.db/dim_sku_info';

insert overwrite table dim.dim_sku_info partition (etl_date = '20231115')
select id,
       spu_id,
       price,
       sku_name,
       sku_desc,
       weight,
       tm_id,
       category3_id,
       sku_default_img,
       is_sale,
       create_time,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.sku_info
where etl_date = '20231115';

with dsku as (
    select id,
           spu_id,
           price,
           sku_name,
           sku_desc,
           weight,
           tm_id,
           category3_id,
           sku_default_img,
           is_sale,
           create_time,
           dwd_insert_user,
           dwd_insert_time,
           dwd_modify_user,
           dwd_modify_time
    from dim.dim_sku_info
    where etl_date = '20231112'
),
     osku as (
         select id,
                spu_id,
                price,
                sku_name,
                sku_desc,
                weight,
                tm_id,
                category3_id,
                sku_default_img,
                is_sale,
                create_time,
                'user1'                                                dwd_insert_user,
                'user1'                                                dwd_modify_user,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
         from ods.sku_info
         where etl_date = '20231113'
     )
select if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.id, dsku.id) did,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.spu_id,
          dsku.spu_id)                                                                                          dspu_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.price,
          dsku.price)                                                                                           dprice,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_name,
          dsku.sku_name)                                                                                        dsku_name,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_desc,
          dsku.sku_desc)                                                                                        dsku_desc,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.weight,
          dsku.weight)                                                                                          dweight,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.tm_id,
          dsku.tm_id)                                                                                           dtm_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.category3_id,
          dsku.category3_id)                                                                                    dcategory3_id,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.sku_default_img,
          dsku.sku_default_img)                                                                                 dsku_default_img,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.is_sale,
          dsku.is_sale)                                                                                         dis_sale,
       if(osku.create_time > nvl(dsku.create_time, cast('1970-01-01 00:00:00' as timestamp)), osku.create_time,
          dsku.create_time)                                                                                     dcreate_time,
       'user1'                                                       as                                         dwd_insert_user,
       nvl(dsku.dwd_insert_time, substr(current_timestamp(), 1, 19)) as                                         dwd_insert_time,
       'user1'                                                       as                                         dwd_modify_user,
       substr(current_timestamp(), 1, 19)                            as                                         dwd_modify_time
from osku
         left join dsku
                   on osku.id = dsku.id
;

show create table ods.base_province;

CREATE TABLE dim.dim_base_province
(
    `id`            bigint,
    `name`          string,
    `region_id`     string,
    `area_code`     string,
    `iso_code`      string,
    `iso_3166_2`    string,
    `create_time`   string,
    dwd_insert_user string,
    dwd_insert_time string,
    dwd_modify_user string,
    dwd_modify_time string
)
    PARTITIONED BY (
        `etl_date` string)
    ROW FORMAT SERDE
        'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
        'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dim.db/dim_base_province'
;


insert into table dim.dim_base_province partition (etl_date = '20231115')
select id,
       name,
       region_id,
       area_code,
       iso_code,
       iso_3166_2,
       create_time,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.base_province
where etl_date = '20231115'
;
with dbp as (
    select id,
           name,
           region_id,
           area_code,
           iso_code,
           iso_3166_2,
           create_time,
           dwd_insert_user,
           dwd_insert_time,
           dwd_modify_user,
           dwd_modify_time
    from dim.dim_base_province
    where etl_date = '20231112'
),
     obp as (
         select id,
                name,
                region_id,
                area_code,
                iso_code,
                iso_3166_2,
                create_time,
                'user1'                                                dwd_insert_user,
                'user1'                                                dwd_modify_user,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
         from ods.base_province
         where etl_date = '20231113'
     )

select `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.id, dbp.id) as id,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.name,
            dbp.name)                                                                                        as name,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.region_id,
            dbp.region_id)                                                                                   as region_id,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.area_code,
            dbp.area_code)                                                                                   as area_code,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.iso_code,
            dbp.iso_code)                                                                                    as iso_code,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.iso_3166_2,
            dbp.iso_3166_2)                                                                                  as iso_3166_2,
       `if`(obp.create_time > nvl(dbp.create_time, cast('1970-01-01 00:00:0' as timestamp)), obp.create_time,
            dbp.create_time)                                                                                 as create_time,
       'user1'                                                                                               as dwd_insert_user,
       nvl(dbp.dwd_insert_time, substr(current_timestamp(), 1, 19))                                          as dwd_insert_time,
       'user1'                                                                                               as dwd_modify_user,
       substr(current_timestamp(), 1, 19)                                                                    as dwd_modify_time
from obp
         left join dbp
                   on obp.id = dbp.id
;

show create table ods.base_region;

CREATE TABLE dim.dim_base_region
(
    `id`            string,
    `region_name`   string,
    `create_time`   string,
    dwd_insert_user string,
    dwd_insert_time string,
    dwd_modify_user string,
    dwd_modify_time string
)
    PARTITIONED BY (
        `etl_date` string)
    ROW FORMAT SERDE
        'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
        'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dim.db/dim_base_region'
;

insert into table dim.dim_base_region partition (etl_date = '20231115')
select id,
       region_name,
       create_time,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.base_region
where etl_date = '20231115'
;

select if(obr.create_time > nvl(dbr.create_time, cast('1970-01-01 00:00:00' as timestamp)), obr.id, dbr.id) as id,
       if(obr.create_time > nvl(dbr.create_time, cast('1970-01-01 00:00:00' as timestamp)), obr.region_name,
          dbr.region_name)                                                                                  as region_name,
       if(obr.create_time > nvl(dbr.create_time, cast('1970-01-01 00:00:00' as timestamp)), obr.create_time,
          dbr.create_time)                                                                                  as create_time,
       'user1'                                                                                              as dwd_insert_user,
       nvl(dbr.dwd_insert_time, substr(current_timestamp(), 1, 19))                                         as dwd_insert_time,
       'user1'                                                                                              as dwd_modify_user,
       substr(current_timestamp(), 1, 19)                                                                   as dwd_modify_time
from (
         select id,
                region_name,
                create_time,
                dwd_insert_user,
                dwd_insert_time,
                dwd_modify_user,
                dwd_modify_time
         from dim.dim_base_region
         where etl_date = '20231112'
     ) dbr
         right join (
    select id,
           region_name,
           create_time,
           'user1'                                                dwd_insert_user,
           'user1'                                                dwd_modify_user,
           from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
           from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
    from ods.base_region
    where etl_date = '20231213'
) obr
                    on dbr.id = obr.id
;




with dui as
         (
             select id,
                    login_name,
                    nick_name,
                    passwd,
                    name,
                    phone_num,
                    email,
                    head_img,
                    user_level,
                    birthday,
                    gender,
                    create_time,
                    operate_time,
                    status,
                    dwd_insert_user,
                    dwd_modify_user,
                    dwd_insert_time,
                    dwd_modify_time
             from dim.dim_user_info
             where etl_date = '20231116'
         ),
     oui as (
         select id,
                login_name,
                nick_name,
                passwd,
                name,
                phone_num,
                email,
                head_img,
                user_level,
                birthday,
                gender,
                create_time,
                operate_time,
                status,
                'user1'                                                dwd_insert_user,
                'user1'                                                dwd_modify_user,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
                from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
         from ods.user_info
         where etl_date = '20231117'
     )

select if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.id, dui.id)                     as id,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.login_name, dui.login_name)     as login_name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.nick_name, dui.nick_name)       as nick_name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.passwd, dui.passwd)             as passwd,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.name, dui.name)                 as name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.phone_num, dui.phone_num)       as phone_num,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.email, dui.email)               as email,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.head_img, dui.head_img)         as head_img,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.user_level, dui.user_level)     as user_level,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.birthday, dui.birthday)         as birthday,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.gender, dui.gender)             as gender,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.create_time, dui.create_time)   as create_time,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.operate_time, dui.operate_time) as operate_time,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.status, dui.status)             as status,
       'user1'                                                                                                                                                                as dwd_insert_user,
       nvl(dui.dwd_insert_time, substr(current_timestamp(), 1, 19))                                                                                                           as dwd_insert_time,
       'user1'                                                                                                                                                                as dwd_modify_user,
       substr(current_timestamp(), 1, 19)                                                                                                                                     as dwd_modify_time
from oui
         left join dui
                   on oui.id = dui.id





;







select
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.id, dui.id)                     as id,  //if条件满足取oui,不满足取dui
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.login_name, dui.login_name)     as login_name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.nick_name, dui.nick_name)       as nick_name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.passwd, dui.passwd)             as passwd,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.name, dui.name)                 as name,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.phone_num, dui.phone_num)       as phone_num,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.email, dui.email)               as email,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.head_img, dui.head_img)         as head_img,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.user_level, dui.user_level)     as user_level,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.birthday, dui.birthday)         as birthday,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.gender, dui.gender)             as gender,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.create_time, dui.create_time)   as create_time,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.operate_time, dui.operate_time) as operate_time,
       if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.status, dui.status)             as status,
       'user1'                                                                                                                                                                as dwd_insert_user,
       nvl(dui.dwd_insert_time, substr(current_timestamp(), 1, 19))                                                                                                           as dwd_insert_time,
       'user1'                                                                                                                                                                as dwd_modify_user,
       substr(current_timestamp(), 1, 19)
from
    (
        select *,
               'user1'                                                dwd_insert_user,
               'user1'                                                dwd_modify_user,
               from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,//这里用这个取时间方法吗  from_unixtime(substr(current_time(),1,19),'yyyy-MM-dd HH:mm:ss') 这样
               from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
        from ods.user_info
        where etl_date = '20231117'
    ) oui
    left join
    (
        select *
        from dim.dim_user_info
        where etl_date = '20231116'
    ) dui
    on oui.id = dui.id;

-- 比赛是2023 11 18

select *

from
    (
        select *,
               'user1'                                                dwd_insert_user,
               'user1'                                                dwd_modify_user,
               from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
               from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time
        from ods.user_info
        where etl_date = '20231117'
    ) oui
        left join
    (
        select *
        from dim.dim_user_info
        where etl_date = '20231116'
    ) dui
    on oui.id = dui.id;



select current_timestamp()  `直接使用current_timestamp`,
       substr(current_timestamp(), 1, 19) `截取current`,
       unix_timestamp() `使用unix_timestamp`,
       from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') `格式化unix_timestamp`


;



-- insert into table dwd.table2 partition (etl_date = '20231227')
select
    if( oui.create_time > nvl(dui.create_time, cast('1970-01-01 00:00:00' as timestamp)), oui.id, dui.id)                     as id,  //if条件满足取oui,不满足取dui
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.login_name, dui.login_name)     as login_name,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.nick_name, dui.nick_name)       as nick_name,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.passwd, dui.passwd)             as passwd,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.name, dui.name)                 as name,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.phone_num, dui.phone_num)       as phone_num,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.email, dui.email)               as email,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.head_img, dui.head_img)         as head_img,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.user_level, dui.user_level)     as user_level,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.gender, dui.gender)             as gender,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.create_time, dui.create_time)   as create_time,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.operate_time, dui.operate_time) as operate_time,
    if(nvl(oui.operate_time, oui.create_time) > nvl(nvl(dui.operate_time, dui.create_time), cast('1970-01-01 00:00:00' as timestamp)), oui.status, dui.status)             as status,
    'user1'                                                                                                                                                                as dwd_insert_user,
    nvl(dui.dwd_insert_time, substr(current_timestamp(), 1, 19))                                                                                                           as dwd_insert_time,
    'user1'                                                                                                                                                                as dwd_modify_user,
    substr(current_timestamp(), 1, 19)                                                                                                                                     as dwd_modify_time

from

    (select *,
            'user1'                                                           dwd_insert_user,
           substr( `current_timestamp`(),1,19)                                              dwd_insert_time,
    'user1'                                                           dwd_modify_user,
    substr(`current_timestamp`(),1,19)                                              dwd_modify_user
from ods.sku_info
where etl_date = '20231227'
) oui

left join

(select *
from dim.dim_sku_info
where etl_time = '20231227')dui
on oui.id = dui.id


;













