package com.xl.competition.modul_b.task1

import org.apache.spark.sql.{Row, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/14 12:40:04
 * @program: com.xl.competition
 * @description: ${description}
 */
object LoadBaseProvinceToOds {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .getOrCreate()

    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "Abc123..")

    spark.read
      .jdbc("jdbc:mysql://node3:3306/shtd_store", "base_province", prop)
      .createTempView("temp_base_province")


    val row: Row = spark.sql(
      """
        |select max(id) from ods.base_province
        |""".stripMargin)
      .take(1)(0)

    var id: Long = 0
    if (!row.isNullAt(0)) {
      id = row.getLong(0)
    }

    spark.sql(
      s"""
         |insert into ods.base_province partition (etl_date = '20231117')
         |select id,
         |       name,
         |       region_id,
         |       area_code,
         |       iso_code,
         |       iso_3166_2,
         |       substr(current_timestamp(),1,19) create_time
         |from temp_base_province
         |where id > $id
         |""".stripMargin)

    spark.stop()
  }
}
