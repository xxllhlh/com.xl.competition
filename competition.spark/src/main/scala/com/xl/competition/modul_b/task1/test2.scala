package com.xl.competition.modul_b.task1

import org.apache.spark.sql.SparkSession

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/15 22:25:48
 * @program: com.xl.competition
 * @description: ${description}
 */
object test2 {
  def main(args: Array[String]): Unit = {
    val t2: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName("test2")
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .getOrCreate()

    val p2 = new Properties()
    p2.put("user", "root")
    p2.put("password", "Abc123..")

    t2.read
      .jdbc("jdbc:mysql://node3:3306/shtd_store", "sku_info",p2)
      .createTempView("temp2")

    val t = t2.sql(
      """
        |select max(unix_timestamp(create_time))
        |from ods.sku_info
        |""".stripMargin)
      .take(1)(0)

    var r = 0L
       if(!t.isNullAt(0))
       r = t.getLong(0)

    t2.sql(
      s"""
        |insert into ods.sku_info partition(etl_date='20231114')
        |select
        |`id` ,
        |  `spu_id` ,
        |  `price` ,
        |  `sku_name` ,
        |  `sku_desc` ,
        |  `weight` ,
        |  `tm_id` ,
        |  `category3_id` ,
        |  `sku_default_img` ,
        |  `is_sale` ,
        |  `create_time`
        |from temp2
        |where unix_timestamp(create_time) > $r
        |""".stripMargin)
  }
}
