package com.xl.competition.modul_b.task1

import org.apache.spark.sql.{Row, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/15 13:01:16
 * @program: com.xl.competition
 * @description: ${description}
 */
object LoadBaseRegionToOds {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .getOrCreate()
    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "Abc123..")

    spark.read
      .jdbc("jdbc:mysql://node3:3306/shtd_store", "base_region", prop)
      .createTempView("temp_base_region")

    val row: Row = spark.sql(
      """
        |select max(id) from ods.base_region
        |""".stripMargin)
      .take(1)(0)

    var id: Int = 0
    if (!row.isNullAt(0)) {
      id = Integer.parseInt(row.getString(0))
    }

    spark.sql(
      s"""
         |insert into ods.base_region partition (etl_date = '20231117')
         | select   id ,
         | region_name string,
         | substr(current_timestamp(),1,19) create_time
         |from temp_base_region
         |where cast(id as int) > $id
         |""".stripMargin)

    spark.stop()

  }
}
