insert into table ods.user_info partition (etl_date = '20231113')
select tui.id,
       tui.login_name,
       tui.nick_name,
       tui.passwd,
       tui.name,
       tui.phone_num,
       tui.email,
       tui.head_img,
       tui.user_level,
       tui.birthday,
       tui.gender,
       tui.create_time,
       tui.operate_time,
       tui.status
from (select id,
             login_name,
             nick_name,
             passwd,
             name,
             phone_num,
             email,
             head_img,
             user_level,
             birthday,
             gender,
             create_time,
             operate_time,
             status,
             `if`(unix_timestamp(create_time) > nvl(unix_timestamp(operate_time), 0),
                  unix_timestamp(create_time),
                  unix_timestamp(operate_time)
                 ) flag
      from ods.user_info
     ) oui
         right join
     (
         select id,
                login_name,
                nick_name,
                passwd,
                name,
                phone_num,
                email,
                head_img,
                user_level,
                birthday,
                gender,
                status,
                create_time,
                operate_time,
                `if`(unix_timestamp(create_time) > nvl(unix_timestamp(operate_time), 0),
                     unix_timestamp(create_time),
                     unix_timestamp(operate_time)
                    ) flag
         from temp_user_info
     ) tui
     on oui.id = tui.id
where nvl(oui.flag, 0) < tui.flag;
select count(*)
from user_info;


select `if`(max(unix_timestamp(create_time)) > max(unix_timestamp(operate_time)), max(unix_timestamp(create_time)),
            max(unix_timestamp(operate_time)))
from ods.user_info;


insert into table ods.sku_info partition (etl_date = "20231113")
select id,
       spu_id,
       price,
       sku_name,
       sku_desc,
       weight,
       tm_id,
       category3_id,
       sku_default_img,
       is_sale,
       create_time
from ods.sku_info
where unix_timestamp(create_time) > $times

select max(id)
from ods.base_province;


insert into base_province partition (etl_date = '20231113')
select id,
       name,
       region_id,
       area_code,
       iso_code,
       iso_3166_2
from ods.base_province;



select count(*)
from ods.user_info
where etl_date = '20231112';

select count(*)
from ods.user_info
where etl_date = '20231113';


select max(unix_timestamp(create_time))
from ods.sku_info;

insert into ods.order_info partition (etl_date = '20231113')
select toi.id,
       toi.consignee,
       toi.consignee_tel,
       toi.total_amount,
       toi.order_status,
       toi.user_id,
       toi.payment_way,
       toi.delivery_address,
       toi.order_comment,
       toi.out_trade_no,
       toi.trade_body,
       toi.create_time,
       toi.operate_time,
       toi.expire_time,
       toi.process_status,
       toi.tracking_no,
       toi.parent_order_id,
       toi.img_url,
       toi.province_id,
       toi.activity_reduce_amount,
       toi.coupon_reduce_amount,
       toi.original_total_amount,
       toi.feight_fee,
       toi.feight_fee_reduce,
       toi.refundable_time
from (
         select id,
                consignee,
                consignee_tel,
                total_amount,
                order_status,
                user_id,
                payment_way,
                delivery_address,
                order_comment,
                out_trade_no,
                trade_body,
                create_time,
                operate_time,
                expire_time,
                process_status,
                tracking_no,
                parent_order_id,
                img_url,
                province_id,
                activity_reduce_amount,
                coupon_reduce_amount,
                original_total_amount,
                feight_fee,
                feight_fee_reduce,
                refundable_time,
                `if`(unix_timestamp(create_time) > nvl(unix_timestamp(operate_time), 0),
                     unix_timestamp(create_time),
                     unix_timestamp(operate_time)
                    ) flag
         from ods.order_info
         where etl_date = '20231112'
     ) ooi
         right join (
    select id,
           consignee,
           consignee_tel,
           total_amount,
           order_status,
           user_id,
           payment_way,
           delivery_address,
           order_comment,
           out_trade_no,
           trade_body,
           create_time,
           operate_time,
           expire_time,
           process_status,
           tracking_no,
           parent_order_id,
           img_url,
           province_id,
           activity_reduce_amount,
           coupon_reduce_amount,
           original_total_amount,
           feight_fee,
           feight_fee_reduce,
           refundable_time,
           `if`(unix_timestamp(create_time) > nvl(unix_timestamp(operate_time), 0),
                unix_timestamp(create_time),
                unix_timestamp(operate_time)
               ) flag
    from order_info
) toi
                    on ooi.id = toi.id
where nvl(ooi.flag, 0) < toi.flag
;

select *
from ods.cart_info;


select unix_timestamp(max(create_time))
from ods.order_detail
where etl_date = '20231112'
;
insert into ods.order_detail partition (etl_date = '20231113')
select id,
       order_id,
       sku_id,
       sku_name,
       img_url,
       order_price,
       sku_num,
       create_time,
       source_type,
       source_id,
       split_total_amount,
       split_activity_amount,
       split_coupon_amount
from order_detail
;

show partitions ods.user_info;
select unix_timestamp(create_time)
from ods.order_info;

select `if`(max(unix_timestamp(create_time)) > max(nvl(unix_timestamp(operate_time), 0L)),
            max(unix_timestamp(create_time)), max(unix_timestamp(operate_time))
           )
from ods.user_info
;

drop table cart_info;
create table cart_info
(
    id           bigint,
    user_id      string,
    sku_id       bigint,
    cart_price   decimal(10, 2),
    sku_num      int,
    img_url      string,
    sku_name     string,
    is_checked   int,
    create_time  timestamp,
    operate_time timestamp,
    is_ordered   bigint,
    order_time   timestamp,
    source_type  string,
    source_id    bigint
)
    partitioned by ( etl_date string)