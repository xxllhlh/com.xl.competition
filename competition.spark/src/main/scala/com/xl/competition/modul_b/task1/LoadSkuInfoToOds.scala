package com.xl.competition.modul_b.task1

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.functions.udf

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/14 11:37:13
 * @program: com.xl.competition
 * @description: ${description}
 */
object LoadSkuInfoToOds {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .getOrCreate()

    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "Abc123..")

    spark.read
      .jdbc("jdbc:mysql://node3:3306/shtd_store", "sku_info", prop)
      .createTempView("temp_sku_info")

    val row: Row = spark.sql(
      """
        |select unix_timestamp(max(create_time))
        |from ods.sku_info
        |""".stripMargin)
      .take(1)(0)

    var times: Long = 0
    if (!row.isNullAt(0)) {
      times = row.getLong(0)
    }

    println(times)

    spark.sql(
      s"""
         |insert into table ods.sku_info partition(etl_date = "20231117")
         |select *
         |from temp_sku_info
         |where unix_timestamp(create_time) > $times
         |""".stripMargin)

    spark.stop()
  }
}
