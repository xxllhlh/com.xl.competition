create database dwd;

use dwd;

show create table ods.order_info;


CREATE TABLE dwd.fact_order_info
(
    `id`                     bigint,
    `consignee`              string,
    `consignee_tel`          string,
    `total_amount`           decimal(10, 2),
    `order_status`           string,
    `user_id`                bigint,
    `payment_way`            string,
    `delivery_address`       string,
    `order_comment`          string,
    `out_trade_no`           string,
    `trade_body`             string,
    `create_time`            string,
    `operate_time`           timestamp,
    `expire_time`            timestamp,
    `process_status`         string,
    `tracking_no`            string,
    `parent_order_id`        bigint,
    `img_url`                string,
    `province_id`            int,
    `activity_reduce_amount` decimal(16, 2),
    `coupon_reduce_amount`   decimal(16, 2),
    `original_total_amount`  decimal(16, 2),
    `feight_fee`             decimal(16, 2),
    `feight_fee_reduce`      decimal(16, 2),
    `refundable_time`        timestamp,
    dwd_insert_user          string,
    dwd_insert_time          string,
    dwd_modify_user          string,
    dwd_modify_time          string

) partitioned by (etl_date string)
    ROW FORMAT SERDE
        'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
        'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dwd/fact_order_info'
;


insert into table dwd.fact_order_info partition (etl_date = '20231115')
select id,
       consignee,
       consignee_tel,
       total_amount,
       order_status,
       user_id,
       payment_way,
       delivery_address,
       order_comment,
       out_trade_no,
       trade_body,
       from_unixtime(unix_timestamp(nvl(create_time, operate_time)),'yyyyMMdd') as create_time,
       operate_time,
       expire_time,
       process_status,
       tracking_no,
       parent_order_id,
       img_url,
       province_id,
       activity_reduce_amount,
       coupon_reduce_amount,
       original_total_amount,
       feight_fee,
       feight_fee_reduce,
       refundable_time,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.order_info
where etl_date = '20231115'
;
show create table ods.order_detail;

CREATE TABLE dwd.fact_order_detail
(
    `id`                    bigint,
    `order_id`              bigint,
    `sku_id`                bigint,
    `sku_name`              string,
    `img_url`               string,
    `order_price`           decimal(10, 2),
    `sku_num`               bigint,
    `create_time`           string,
    `source_type`           string,
    `source_id`             bigint,
    `split_total_amount`    decimal(16, 2),
    `split_activity_amount` decimal(16, 2),
    `split_coupon_amount`   decimal(16, 2),
    dwd_insert_user         string,
    dwd_insert_time         string,
    dwd_modify_user         string,
    dwd_modify_time         string
)
    PARTITIONED BY (
        `etl_date` string)
    ROW FORMAT SERDE
        'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
        'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        OUTPUTFORMAT
            'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
        'hdfs://node1:9000/user/hive/warehouse/dwd/order_detail'
;

insert into table dwd.fact_order_detail partition (etl_date = '20231115')
select id,
       order_id,
       sku_id,
       sku_name,
       img_url,
       order_price,
       sku_num,
       from_unixtime(unix_timestamp(create_time),'yyyyMMdd') as create_time,
       source_type,
       source_id,
       split_total_amount,
       split_activity_amount,
       split_coupon_amount,
       'user1'                            dwd_insert_user,
       substr(current_timestamp(), 1, 19) dwd_insert_time,
       'user1'                            dwd_modify_user,
       substr(current_timestamp(), 1, 19) dwd_modify_time
from ods.order_detail
where etl_date = '20231115';
