-- 创建学生表
create database if not exists teach;
use teach;
DROP TABLE IF EXISTS student_info;
create table if not exists student_info
(
    stu_id   string COMMENT '学生id',
    stu_name string COMMENT '学生姓名',
    birthday string COMMENT '出生日期',
    sex      string COMMENT '性别'
)
    row format delimited fields terminated by ','
    stored as textfile;

-- 创建课程表
DROP TABLE IF EXISTS course_info;
create table if not exists course_info
(
    course_id   string COMMENT '课程id',
    course_name string COMMENT '课程名',
    tea_id      string COMMENT '任课老师id'
)
    row format delimited fields terminated by ','
    stored as textfile;

-- 创建老师表
DROP TABLE IF EXISTS teacher_info;
create table if not exists teacher_info
(
    tea_id   string COMMENT '老师id',
    tea_name string COMMENT '老师姓名'
)
    row format delimited fields terminated by ','
    stored as textfile;

-- 创建分数表
DROP TABLE IF EXISTS score_info;
create table if not exists score_info
(
    stu_id    string COMMENT '学生id',
    course_id string COMMENT '课程id',
    score     int COMMENT '成绩'
)
    row format delimited fields terminated by ','
    stored as textfile;

select s.stu_id,
       s.stu_name,
       t1.score
from student_info s
         join (
    select *
    from score_info
    where course_id = (select course_id from course_info where course_name = '数学')
      and score < 60
) t1 on s.stu_id = t1.stu_id
order by s.stu_id;

select *
from teach.course_info;

select course_id as id,
       course_name name
from course_info;


--获取