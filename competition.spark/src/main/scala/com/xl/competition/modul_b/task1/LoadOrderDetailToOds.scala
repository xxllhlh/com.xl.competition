package com.xl.competition.modul_b.task1

import org.apache.spark.sql.{Row, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/15 13:55:01
 * @program: com.xl.competition
 * @description: ${description}
 */
object LoadOrderDetailToOds {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .getOrCreate()

    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "Abc123..")

    spark.read
      .jdbc("jdbc:mysql://node3:3306/shtd_store", "order_detail", prop)
      .createTempView("temp_order_detail")

    val row: Row = spark.sql(
      """
        |select max(unix_timestamp(create_time))
        |from ods.order_detail
        |""".stripMargin)
      .take(1)(0)

    var times: Long = 0
    if (!row.isNullAt(0)) {
      times = row.getLong(0)
    }

    spark.sql(
      s"""
         |insert into ods.order_detail partition(etl_date = '20231117')
         |select *
         |from temp_order_detail
         |where unix_timestamp(create_time) > $times
         |""".stripMargin)


    spark.stop()
  }
}
