package com.xl.competition.old.task1.f1

import org.apache.spark.sql.SparkSession

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/9 21:33:26
 * @program: com.xl.competition
 */
object DataLoadPartsupp {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val sc: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://master:9083")
      .getOrCreate()

    val properties = new Properties()
    properties.put("user", "root")
    properties.put("password", "Abc123..")

    sc
      .read
      .jdbc("jdbc:mysql://192.168.0.1:3306/competition", "partsupp", properties)
      .createTempView("tmp_partsupp")

    sc.sql(
      """
        |create table if not exists ods.partsupp
        |(
        |    `partkey`    int,
        |    `suppkey`    int,
        |    `availqty`   string,
        |    `supplycost` decimal(10, 2),
        |    `times`      bigint
        |) PARTITIONED BY (`dt` STRING)
        |    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |        NULL DEFINED AS ''
        |    LOCATION '/warehouse/competition/ods/ods_partsupp/'
        |""".stripMargin)

    sc.sql(
      """
        |
        |insert overwrite table ods.partsupp partition (dt = '20231108')
        |select partkey,
        |       suppkey,
        |       availqty,
        |       supplycost,
        |       times
        |from tmp_partsupp
        |""".stripMargin)

    sc.stop()
  }
}
