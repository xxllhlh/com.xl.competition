package com.xl.competition.old.test

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{Result, Scan}
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableInputFormat
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

/**
 * @author: xl
 * @createTime: 2023/12/13 19:58:50
 * @program: com.xl.competition
 * @description: spark读取hbase数据测试
 */
object SparkReadHbaseTest {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://node2:9083")
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .getOrCreate()
    read(spark, "node1", "2181", "pv_weekday_7d", "info", Seq("pv", "fav", "buy")).show()
    spark.stop()
  }

  def read(spark: SparkSession, zks: String, port: String, table: String,
           family: String, fields: Seq[String]): DataFrame = {

    // 设置HBase中Zookeeper集群信息
    val conf: Configuration = new Configuration()
    conf.set("hbase.zookeeper.quorum", zks)
    conf.set("hbase.zookeeper.property.clientPort", port)
    // 设置读HBase表的名称
    conf.set(TableInputFormat.INPUT_TABLE, table)
    // 设置读取列簇和列名称
    val scan: Scan = new Scan()
    // 设置列簇
    val familyBytes: Array[Byte] = Bytes.toBytes(family)
    scan.addFamily(familyBytes)
    // 设置列名称
    fields.foreach { field =>
      scan.addColumn(familyBytes, Bytes.toBytes(field))
    }

    // 调用底层API，读取HBase表的数据
    val datasRDD: RDD[(ImmutableBytesWritable, Result)] =
      spark.sparkContext
        .newAPIHadoopRDD(
          conf,
          classOf[TableInputFormat],
          classOf[ImmutableBytesWritable],
          classOf[Result]
        )
    // 解析转换为DataFrame
    val rowsRDD: RDD[Row] = datasRDD.map { case (_, result) =>
      // 列的值
      val values: Seq[String] = fields.map { field =>
        Bytes.toString(result.getValue(familyBytes, Bytes.toBytes(field)))
      }
      val row: String = Bytes.toString(result.getRow)

      // 生成Row对象
      Row.fromSeq(row ++ values)
    }
    // 定义Schema信息
    val structType: StructType = new StructType()
    structType.add(StructField("rowkey", StringType))
    fields.foreach { field =>
      structType.add(StructField(field, StringType, nullable = true))
    }
    // 转换为DataFrame
    spark.createDataFrame(rowsRDD, structType)
  }
}
