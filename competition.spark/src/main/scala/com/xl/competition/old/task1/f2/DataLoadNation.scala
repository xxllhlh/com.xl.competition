package com.xl.competition.old.task1.f2

import org.apache.spark.sql.{DataFrame, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/9 19:29:18
 * @program: com.xl.competition
 */
object DataLoadNation {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val sc: SparkSession = SparkInitUtils.getSparkSession

    SparkInitUtils.getDataFrame("nation", sc).createTempView("tmp_nation")

    sc.sql(
      """
        | CREATE TABLE if not exists ods.nation
        | (
        |     `nationkey` int,
        |     `NAME`      string,
        |     `regionkey` int,
        |     `times`     bigint
        | )
        |     PARTITIONED BY (`dt` STRING)
        |     ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |         NULL DEFINED AS ''
        |     LOCATION '/warehouse/competition/ods/ods_nation/'
        |""".stripMargin)

    sc.sql(
      """
        |insert overwrite table ods.nation partition (dt = '20231108')
        |select  `nationkey`,
        |        `NAME`    ,
        |        `regionkey`,
        |        `times`
        |from tmp_nation
        |""".stripMargin)

    sc.stop()
  }

}
