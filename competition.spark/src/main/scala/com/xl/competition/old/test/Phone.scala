package com.xl.competition.old.test

/**
 * Author: xl
 * Date: 2022/4/20 9:43
 * program: com.xl.competition
 * Email: 2199396150@qq.com
 * Description: ${description}
 */
object Phone {
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn
    //定义一个list
    val phone = List(
      "15,1300014,天津,天津,中国联通,022,300000",
      "16,1300015,山东,淄博,中国联通,0533,255000",
      "19,1300018,天津,天津,中国联通,022,300000",
      "20,1300019,天津,天津,中国联通,022,300000",
      "21,1300020,上海,上海,中国联通,021,200000",
      "22,1300021,上海,上海,中国联通,021,200000")
    //控制台输入
    val place: String = StdIn.readLine("请输入你要查询的地区：")
    val ares: Array[String] = place.split(",")
    //省份
    val province: String = ares(0)
    //城市
    val city: String = ares(1)
    println(phoneCount(phone, province, city))
    area2phone(phone, province, city)
  }

  //  统计某个地区的号码数量
  def phoneCount(list: List[String], province: String, city: String): Map[Iterable[(String, String)], Int] = {
    list.map(x => {
      val arr = x.split(",")
      Map((arr(2), arr(3)) -> 1)
    }).filter(x => x.contains(province, city))
      .groupBy(item => {
        item.keys
      }).map(x => {(x._1,x._2.size)})
  }

  //  查询某个地区的号码
  def area2phone(list: List[String], province: String, city: String): Unit = {
    val distinct: List[Map[(String, String), String]] = list.map(x => {
      val arr = x.split(",")
      Map((arr(2), arr(3)) -> arr(1))
    }).filter(x => x.contains(province, city))
      .distinct
    println(distinct)
  }
}
