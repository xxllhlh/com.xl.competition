package com.xl.competition.old.task1.f2

import org.apache.spark.sql.{DataFrame, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/10 13:20:59
 * @program: com.xl.competition
 * @description: ${description}
 */
object SparkInitUtils {
  def getSparkSession: SparkSession = {
    SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://master:9083")
      .getOrCreate()
  }

  def getDataFrame(tableName: String, sc: SparkSession): DataFrame = {
    val properties = new Properties()
    properties.put("user", "root")
    properties.put("password", "Abc123..")
    
    sc
      .read
      .jdbc("jdbc:mysql://192.168.0.1:3306/competition", s"$tableName", properties)
  }
}
