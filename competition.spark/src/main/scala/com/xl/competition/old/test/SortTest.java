package com.xl.competition.old.test;

import java.util.Arrays;

/**
 * Author: xl
 * Date: 2022/4/9 23:01
 * program: com.xl.competition
 * Email: 2199396150@qq.com
 * Description:copilot排序测试
 */
public class SortTest {
    public static void main(String[] args) {
        int[] arr = {8, 5, 3, 6, 1, 9, 10};
        quickSort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    //    快速排序
    public static void quickSort(int[] arr, int left, int right) {
        if (left < right) {
            int pivot = partition(arr, left, right);
            quickSort(arr, left, pivot - 1);
            quickSort(arr, pivot + 1, right);
        }
    }

    //    分区
    public static int partition(int[] arr, int left, int right) {
        int pivot = arr[right];
        int i = left;
        for (int j = left; j < right; j++) {
            if (arr[j] > pivot) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, right);
        return i;
    }

    //    交换
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
