package com.xl.competition.old.task1.f2

import org.apache.spark.sql.SparkSession

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/7 16:48:09
 * @program: com.xl.competition
 */
object DataLoadCustomer {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val sc: SparkSession = SparkInitUtils.getSparkSession

    SparkInitUtils.getDataFrame("customer", sc).createTempView("tmp_customer")

    sc.sql(
      """
        |create table IF NOT EXISTS ods.customer
        |(
        |    `custkey`    int,
        |    `NAME`       string,
        |    `gender`     string,
        |    `address`    string,
        |    `nationkey`  int,
        |    `phone`      string,
        |    `mktsegment` string,
        |    `acctbal`    decimal(12, 2),
        |     times        bigint,
        |    `datime`     string
        |)
        |    PARTITIONED BY (`dt` STRING)
        |    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |        NULL DEFINED AS ''
        |    LOCATION '/warehouse/competition/ods/ods_customer/'
        |""".stripMargin)

    sc.sql(
      """
        |insert overwrite table ods.customer partition (dt = '20231108')
        |select custkey,
        |       name,
        |       gender,
        |       address,
        |       nationkey,
        |       phone,
        |       mktsegment,
        |       acctbal,
        |       times,
        |       datime
        |from tmp_customer
        |""".stripMargin)

    sc.stop()
  }
}
