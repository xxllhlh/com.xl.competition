package com.xl.competition.old.task1.f1

import org.apache.spark.sql.SparkSession

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/7 16:48:09
 * @program: com.xl.competition
 */
object DataLoadCustomer {
  def main(args: Array[String]): Unit = {

    System.setProperty("HADOOP_USER_NAME", "root")
    //初始化sparkSession对象

    val sc: SparkSession = SparkSession
      .builder() //利用builder方法构建
      .master("local[*]") //本地模式运行下的线程资源  提交集群时不写
      .appName(this.getClass.getName) //应用名字  方便日志跟踪
      .enableHiveSupport() //支持hive   因为要写到hive表中
      .config("hive.metastore.uris", "thrift://master:9083") //hive的元数据地址 提交集群可不写但要有hive的环境变量
      .getOrCreate()

    //MYSQL的连接信息
    val properties = new Properties()
    properties.put("user", "root") //用户
    properties.put("password", "Abc123..") //密码

    sc
      .read
      .jdbc("jdbc:mysql://192.168.0.1:3306/competition", "customer", properties)
      .createTempView("tmp_customer") //创建临时视图
    //先在hive中建表 如果hive中已经存在可忽略
    sc.sql(
      """
        |create table IF NOT EXISTS ods.customer
        |(
        |    `custkey`    int,
        |    `NAME`       string,
        |    `gender`     string,
        |    `address`    string,
        |    `nationkey`  int,
        |    `phone`      string,
        |    `mktsegment` string,
        |    `acctbal`    decimal(12, 2),
        |    `times`      bigint,
        |    `datime`     string
        |)
        |    PARTITIONED BY (`dt` STRING)
        |    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |        NULL DEFINED AS ''
        |    LOCATION '/warehouse/competition/ods/ods_customer/'
        |""".stripMargin)

    //执行插入语句  从临时视图中读取数据写入到ods.customer中
    sc.sql(
      """
        |insert overwrite table ods.customer partition (dt = '20231108')
        |select custkey,
        |       name,
        |       gender,
        |       address,
        |       nationkey,
        |       phone,
        |       mktsegment,
        |       acctbal,
        |       times,
        |       datime
        |from tmp_customer
        |""".stripMargin)

    sc.stop()
  }
}
