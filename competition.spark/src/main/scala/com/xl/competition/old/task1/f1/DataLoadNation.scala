package com.xl.competition.old.task1.f1

import org.apache.spark.sql.{DataFrame, SparkSession}

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/9 19:29:18
 * @program: com.xl.competition
 */
object DataLoadNation {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val sc: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://master:9083")
      .getOrCreate()

    val properties = new Properties()
    properties.put("user", "root")
    properties.put("password", "Abc123..")

    sc.read
      .jdbc("jdbc:mysql://192.168.0.1:3306/competition", "nation", properties)
      .createTempView("tmp_nation")

    sc.sql(
      """
        | CREATE TABLE if not exists ods.nation
        | (
        |     `nationkey` int,
        |     `NAME`      string,
        |     `regionkey` int,
        |     `times`     bigint
        | )
        |     PARTITIONED BY (`dt` STRING)
        |     ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |         NULL DEFINED AS ''
        |     LOCATION '/warehouse/competition/ods/ods_nation/'
        |""".stripMargin)

    sc.sql(
      """
        |insert overwrite table ods.nation partition (dt = '20231108')
        |select  `nationkey`,
        |        `NAME`    ,
        |        `regionkey`,
        |        `times`
        |from tmp_nation
        |""".stripMargin)

    sc.stop()
  }

}
