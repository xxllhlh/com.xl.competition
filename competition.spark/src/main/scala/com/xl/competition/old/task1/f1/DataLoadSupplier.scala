package com.xl.competition.old.task1.f1

import org.apache.spark.sql.SparkSession

import java.util.Properties

/**
 * @author: xl
 * @createTime: 2023/11/9 21:55:32
 * @program: com.xl.competition
 */
object DataLoadSupplier {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val sc: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName(this.getClass.getName)
      .enableHiveSupport()
      .config("hive.metastore.uris", "thrift://master:9083")
      .getOrCreate()

    val properties = new Properties()
    properties.put("user", "root")
    properties.put("password", "Abc123..")

    sc
      .read
      .jdbc("jdbc:mysql://192.168.0.1:3306/competition", "supplier", properties)
      .createTempView("tmp_supplier")

    sc.sql(
      """
        |create table if not exists ods.supplier
        |(
        |    `suppkey` int,
        |    `NAME` string,
        |    `address` string,
        |    `nationkey` int,
        |    `phone` string,
        |    `acctbal` decimal(12, 2) ,
        |    `times` bigint
        |)PARTITIONED BY (`dt` STRING)
        |    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        |        NULL DEFINED AS ''
        |    LOCATION '/warehouse/competition/ods/ods_supplier/'
        |""".stripMargin)

    //    from_unixtime(unix_timestamp(date_sub(current_date,1)),'yyyyMMdd') as dt
    sc.sql(
      """
        |insert overwrite table ods.supplier partition (dt = '20231108')
        |select SUPPKEY,
        |       NAME,
        |       ADDRESS,
        |       NATIONKEY,
        |       PHONE,
        |       ACCTBAL,
        |       TIMES
        |from tmp_supplier
        |""".stripMargin)

    sc.stop()
  }
}
