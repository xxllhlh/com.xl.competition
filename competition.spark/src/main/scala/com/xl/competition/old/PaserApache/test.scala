package com.xl.competition.old.PaserApache

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Author: xl
 * Date: 2022/3/7 9:54
 * program: com.xl.competition
 * Email: 2199396150@qq.com
 * Description: ${description}
 */
object test {
  def main(args: Array[String]): Unit = {
    val str: Long = 1511668800000L
    val timestamp: Timestamp = Timestamp.valueOf("2020-1-1 20:21:12")
    val time: Long = timestamp.getTime
    println(time)
  }
}
