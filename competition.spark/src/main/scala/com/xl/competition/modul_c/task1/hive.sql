show tables;

--计算出与用户 id 为 6708 的用户所购买相同商品种类最多的前 10 位用户
select user_id,
       count(fod.id) cnt
from fact_order_detail fod
         join fact_order_info foi on foi.id = fod.order_id
where fod.sku_id in
      (
          select distinct fod.sku_id
          from dwd.fact_order_detail fod
                   join dwd.fact_order_info foi
                        on fod.order_id = foi.id
          where user_id = 67)
and user_id != 67
group by user_id
order by cnt desc
limit 10