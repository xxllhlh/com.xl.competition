create database dim;

use dim;
show tables;


create table if not exists dim.dim_customer
(
    `custkey`       int,
    `NAME`          string,
    `gender`        string,
    `address`       string,
    `nationkey`     int,
    `phone`         string,
    `mktsegment`    string,
    `acctbal`       decimal(12, 2),
    `times`         bigint,
    `datime`        string,
    dwd_insert_user string,
    dwd_insert_time string,
    dwd_modify_user string,
    dwd_modify_time string
) PARTITIONED BY (`etldate` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/dim/dim_customer/'
    TBLPROPERTIES ('orc.compress' = 'snappy')
;

select custkey,
       name,
       gender,
       address,
       nationkey,
       phone,
       mktsegment,
       acctbal,
       times,
       `if`(length(datime)==10,concat(datime," 00:00:00"),datime),
       'user1'                                     dwd_insert_user,
       'user1'                                     dwd_modify_user,
       from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_insert_time,
       from_unixtime(unix_timestamp(), 'yyyy-MM-dd HH:mm:ss') dwd_modify_time,
       substring(datime, 0, 10)                    etldate
from ods.customer