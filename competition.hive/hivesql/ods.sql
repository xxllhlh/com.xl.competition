create database ods;

show databases;

drop table ods.customer;
create table IF NOT EXISTS ods.customer
(
    `custkey`    int,
    `NAME`       string,
    `gender`     string,
    `address`    string,
    `nationkey`  int,
    `phone`      string,
    `mktsegment` string,
    `acctbal`    decimal(12, 2),
    `times`      bigint,
    `datime`     string
)
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_customer/'
;



insert overwrite table ods.customer partition (`dt`)
select custkey,
       name,
       gender,
       address,
       nationkey,
       phone,
       mktsegment,
       acctbal,
       times,
       datime,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from ods.customer;


show partitions ods.customer;


DROP TABLE IF EXISTS nation;
CREATE TABLE if not exists ods.nation
(
    `nationkey` int,
    `NAME`      string,
    `regionkey` int,
    `times`     bigint
)
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_nation/';


insert overwrite table ods.nation partition (`dt`)
select `nationkey`,
       `NAME`,
       `regionkey`,
       `times`,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from ods.nation;


create table if not exists ods.part
(
    `partkey`     int,
    `NAME`        string,
    `mfgr`        string,
    `brand`       string,
    `TYPE`        string,
    `size`        string,
    `container`   string,
    `retailprice` decimal(12, 2),
    `times`       bigint
) PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_part/'
;


insert overwrite table ods.part partition (dt)
select partkey,
       name,
       mfgr,
       brand,
       type,
       size,
       container,
       retailprice,
       times,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from ods.part
;

create table if not exists ods.partsupp
(
    `partkey`    int,
    `suppkey`    int,
    `availqty`   string,
    `supplycost` decimal(10, 2),
    `times`      bigint
) PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_partsupp/'
;


insert overwrite table ods.partsupp partition (dt)
select partkey,
       suppkey,
       availqty,
       supplycost,
       times,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from partsupp
;

create table if not exists ods.region
(
    `regionkey` int,
    `NAME`      string
) PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_region/'
;

insert overwrite table ods.region partition (dt)
select regionkey,
       NAME,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from region
;
create table if not exists ods.supplier
(
    `suppkey`   int,
    `NAME`      string,
    `address`   string,
    `nationkey` int,
    `phone`     string,
    `acctbal`   decimal(12, 2),
    `times`     bigint
) PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_supplier/'
;

insert overwrite table ods.supplier partition (dt)
select SUPPKEY,
       NAME,
       ADDRESS,
       NATIONKEY,
       PHONE,
       ACCTBAL,
       TIMES,
       from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd') as dt
from supplier
;

CREATE TABLE if not exists ods.`lineitem`
(
    `linekey`       int,
    `orderkey`      int,
    `partkey`       int,
    `suppkey`       int,
    `linenumber`    int,
    `quantity`      double,
    `extendedprice` decimal,
    `discount`      double,
    `tax`           double,
    `returnflag`    string,
    `linestatus`    string,
    `shipdate`      string,
    `commitdate`    varchar(32),
    `receiptdate`   string,
    `shipinstruct`  string,
    `shipmode`      string
)
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_lineitem/'
;
insert overwrite table ods.lineitem partition (dt = '20231108')
select linekey,
       orderkey,
       partkey,
       suppkey,
       linenumber,
       quantity,
       extendedprice,
       discount,
       tax,
       returnflag,
       linestatus,
       cast(shipdate as string),
       commitdate,
       receiptdate,
       shipinstruct,
       shipmode
from ods.lineitem
;

CREATE TABLE if not exists ods.`orders`
(
    `orderkey`      int,
    `custkey`       int,
    `orderstatus`   string,
    `totalprice`    decimal,
    `orderdate`     string,
    `orderpriority` string,
    `clerk`         int,
    `shippriority`  string
) PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/competition/ods/ods_orders/'
;

insert overwrite table ods.orders partition (dt = '20231108')
select orderkey,
       custkey,
       orderstatus,
       totalprice,
       orderdate,
       orderpriority,
       clerk,
       shippriority
from ods.orders;


select date_sub('2012-12-08', 10);
select from_unixtime(unix_timestamp(date_sub(current_date, 1)), 'yyyyMMdd');

select unix_timestamp();

create table test
(
    id date
);
desc formatted test;


select orderkey,
       custkey,
       orderstatus,
       totalprice,
       orderdate,
       orderpriority,
       clerk,
       shippriority
from orders
where nvl(unix_timestamp(orderdate, "yyyy-MM-dd"), unix_timestamp(orderdate, "yyyy-MM-dd HH:mm:SS")) >
      unix_timestamp('2023-11-09 13:01:03')
;
select nvl(unix_timestamp("2023-11-12", "yyyy-MM-dd"), unix_timestamp("2023-11-12", "yyyy-MM-dd HH:mm:SS")) >
       unix_timestamp('2023-11-09 13:01:03');

select nvl(unix_timestamp(orderdate, "yyyy-MM-dd"), unix_timestamp(orderdate, "yyyy-MM-dd HH:mm:SS"))
from ods.orders;
select max(orderkey)
from orders
limit 1;

-- partition 后面的分区字段是写死了的就是静态分区   如果是写的下面的选择字段那就是动态分区
insert overwrite table ods.orders partition (orderdate)
select orderkey,
       custkey,
       orderstatus,
       totalprice,
       orderdate,
       orderpriority,
       clerk,
       shippriority,
       dt
from orders
where orderkey < 100
;

insert overwrite table ods.orders partition (dt = '20231108')
select orderkey,
       custkey,
       orderstatus,
       totalprice,
       from_unixtime(
               nvl(
                       unix_timestamp(orderdate, "yyyy-MM-dd"),
                       unix_timestamp(orderdate, "yyyy-MM-dd HH:mm:ss")
                   ), 'yyyy-MM-dd') orderdate,
       orderpriority,
       clerk,
       shippriority
from tmp_orders
where orderkey > ${oldKey}
  and nvl(unix_timestamp(orderdate, "yyyy-MM-dd"), unix_timestamp(orderdate, "yyyy-MM-dd HH:mm:ss")) >
      unix_timestamp('2023-11-08 13:01:03');

select `if`(length('2023-11-12') == 10, "2023-11-12 00:00:00", "2023-11-12 12:00:21")
select unix_timestamp("2023-11-12", "yyyy-MM-dd");

select from_unixtime(1699746214982)