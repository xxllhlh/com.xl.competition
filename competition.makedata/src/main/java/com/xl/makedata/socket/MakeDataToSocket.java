package com.xl.makedata.socket;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson2.JSON;
import com.xl.makedata.db.utils.HanZiUtil;
import com.xl.makedata.socket.bean.OrderDetail;
import com.xl.makedata.socket.bean.OrderInfo;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author: xl
 * @createTime: 2023/10/7 20:06:23
 * @program: com.xl.competition
 * @description: 实时产生数据到socket 26001端口
 */
public class MakeDataToSocket {

    public static final Random RANDOM = new Random();
    public static final String[] ORDER_STATUS = new String[]{"finish", "cancel", "refund", "fail"};

    static long date = new Date().getTime();
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws InterruptedException {
        String serverAddress = "node1"; // 服务器地址
        int serverPort = 10050; // 服务器端口
        new Thread(() -> {
            try {
                ServerSocket serverSocket = new ServerSocket(serverPort);

            } catch (Exception ignore) {

            }
        }).start();
        new Thread(() -> {
            try {
                // 创建Socket对象并连接到服务器
                Socket socket = new Socket(serverAddress, serverPort);

                // 获取输出流，用于向服务器发送数据
                OutputStream outputStream = socket.getOutputStream();
                PrintWriter out = new PrintWriter(outputStream, true);

                // 创建一个循环以持续不断地发送数据
                while (true) {
                    // 发送消息
                    out.println(JSONObject.toJSONString(makeOrderInfo(), SerializerFeature.WriteMapNullValue));
                    Thread.sleep(300);
                    for (int i = 0; i < RANDOM.nextInt(5); i++) {
                        out.println(JSONObject.toJSONString(makeOrderDetail(), SerializerFeature.WriteMapNullValue));
                        Thread.sleep(100);
                    }
                    Thread.sleep(200);
                }
            } catch (IOException | InterruptedException | ParseException e) {
                e.printStackTrace();
            }
        }).start();


    }

    private static OrderInfo makeOrderInfo() throws ParseException {
        OrderInfo orderInfo = new OrderInfo();
        long id = RANDOM.nextInt(10000);
        long userId = RANDOM.nextInt(10000);
        String orderStatus = ORDER_STATUS[RANDOM.nextInt(ORDER_STATUS.length)];


        Date createTime = dateFormat.parse(dateFormat.format(date));
        Date operateTime = null;
        if (id % 6 == 0) {
            long opDate = date + (RANDOM.nextInt(400000000) + 1L);
            operateTime = dateFormat.parse(dateFormat.format(opDate));
        }
        Integer provinceId = RANDOM.nextInt(34) + 1;
        String consignee = HanZiUtil.getUserName();
        String consigneeTel = HanZiUtil.getTel();
        BigDecimal totalAmount = HanZiUtil.getRandomRedPacketBetweenMinAndMax(new BigDecimal("0.5"), new BigDecimal("10000"));
        BigDecimal feightFee = HanZiUtil.getRandomRedPacketBetweenMinAndMax(new BigDecimal("0.1"), new BigDecimal("1000"));

        orderInfo.setId(id);
        orderInfo.setUserId(userId);
        orderInfo.setProvinceId(provinceId);
        orderInfo.setConsignee(consignee);
        orderInfo.setConsigneeTel(consigneeTel);
        orderInfo.setCreateTime(createTime);
        orderInfo.setOperateTime(operateTime);
        orderInfo.setTotalAmount(totalAmount);
        orderInfo.setOrderStatus(orderStatus);
        orderInfo.setFeightFee(feightFee);
        return orderInfo;
    }

    private static OrderDetail makeOrderDetail() throws ParseException {
        OrderDetail orderDetail = new OrderDetail();
        Long id = RANDOM.nextInt(1000) + 1L;
        Long orderId = RANDOM.nextInt(10000) + 1L;
        Long skuId = RANDOM.nextInt(10000) + 1L;
        String skuName = HanZiUtil.getRandomManufacturerName();
        BigDecimal orderPrice = HanZiUtil.getRandomRedPacketBetweenMinAndMax(new BigDecimal("0.1"), new BigDecimal("1000"));
        Date createTime = dateFormat.parse(dateFormat.format(date));
        Date operateTime = null;
        if (id % 6 == 0) {
            long opDate = date + (RANDOM.nextInt(400000000) + 1L);
            operateTime = dateFormat.parse(dateFormat.format(opDate));
        }
        BigDecimal splitTotalAmount = HanZiUtil.getRandomRedPacketBetweenMinAndMax(new BigDecimal("0.1"), new BigDecimal("2000"));
        BigDecimal splitCouponAmount = HanZiUtil.getRandomRedPacketBetweenMinAndMax(new BigDecimal("0.5"), new BigDecimal("4000"));

        orderDetail.setId(id);
        orderDetail.setOrderId(orderId);
        orderDetail.setSkuId(skuId);
        orderDetail.setSkuName(skuName);
        orderDetail.setOrderPrice(orderPrice);
        orderDetail.setCreateTime(createTime);
        orderDetail.setOperateTime(operateTime);
        orderDetail.setSplitTotalAmount(splitTotalAmount);
        orderDetail.setSplitCouponAmount(splitCouponAmount);
        return orderDetail;
    }
}
