package com.xl.makedata.socket.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: xl
 * @createTime: 2023/10/7 20:10:43
 * @program: com.xl.competition
 * @description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo {
    private Long id;
    private String consignee;
    private String consigneeTel;
    private BigDecimal totalAmount;
    private String orderStatus;
    private Long userId;
    private String deliveryAddress;
    private String orderComment;
    private String outTradeNo;
    private String tradeBody;
    private Date createTime;
    private Date operateTime;
    private Date expireTime;
    private String trackingNo;
    private Long parentOrderId;
    private String imgUrl;
    private Integer provinceId;
    private BigDecimal originalTotalAmount;
    private BigDecimal feightFee;
    private BigDecimal activityReduceAmount;
    private BigDecimal couponReduceAmount;
}
