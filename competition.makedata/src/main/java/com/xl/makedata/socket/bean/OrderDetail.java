package com.xl.makedata.socket.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: xl
 * @createTime: 2023/11/19 12:15:38
 * @program: com.xl.competition
 * @description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail {
    private Long id;
    private Long orderId;
    private Long skuId;
    private String skuName;
    private String imgUrl;
    private BigDecimal orderPrice;
    private Long skuNum;
    private Date createTime;
    private Date operateTime;
    private BigDecimal splitTotalAmount;
    private BigDecimal splitCouponAmount;
    private BigDecimal splitActivityAmount;
    private String sourceType;
    private Long sourceId;
}
