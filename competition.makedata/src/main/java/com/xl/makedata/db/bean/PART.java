package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import com.xl.makedata.db.utils.HanZiUtil;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

;

public class PART implements Run {
    private int partkey = 1;
    private String NAME;
    //    制作商
    private String mfgr;
    //    品牌
    private String brand;
    private String type;
    private int size;
    //    容器
    private String container;
    //    零售价
    private BigDecimal retailprice;
    private long timestamp;


    public void run(int range) throws SQLException, ParseException {
        String sql = "replace into part(partkey, NAME, mfgr, brand, TYPE, size, container, retailprice, times) values(?, ? ,?,?,?,?,?,?,?)";


        new BaseJdbcSink(sql) {

            @Override
            public void Write2Mysql() throws Exception {
                partkey = getLastId("part", "partkey") + 1;
                this.ps = connection.prepareStatement(this.sql);
                for (int i = 1; i <= range; i++) {
                    makeData(1L);
                        ps.setInt(1, partkey);
                        ps.setString(2, NAME);
                        ps.setString(3, mfgr);
                        ps.setString(4, brand);
                        ps.setString(5, type);
                        ps.setInt(6, size);
                        ps.setString(7, container);
                        ps.setBigDecimal(8, retailprice);
                        ps.setLong(9, timestamp);
                        ps.execute();
                        partkey++;
                }
            }
        };
    }

    @Override
    public void makeData(Long set) throws ParseException {
        Random random = new Random();
        this.NAME = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(2) + 1);
        this.mfgr = HanZiUtil.getRandomManufacturerName();
        this.brand = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(2) + 1);
        this.type = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(2) + 1);
        this.size = random.nextInt(10) + 1;
        this.container = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(3) + 1);
        this.retailprice = BigDecimal.valueOf(random.nextDouble() * 100);
        int flag = random.nextInt(10) + 1;
        long date = new Date().getTime();
        date -= flag * 100000000L;
        if (flag % 2 == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        }
    }
}
