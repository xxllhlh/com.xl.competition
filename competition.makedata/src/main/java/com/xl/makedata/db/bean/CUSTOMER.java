package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import com.xl.makedata.db.utils.HanZiUtil;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 消费者表
 */
public class CUSTOMER implements Run {
    //主键
    private int custkey;
    //姓名
    private String NAME;
    //性别
    private String gender;
    //住址
    private String address;
    //国家id
    private int nationkey;
    //电话号码
    private String phone;
    //商品名
    private String mktsegment;
    //购买金额
    private BigDecimal acctbal;
    //时间
    private String datetime;
    //时间戳
    private long timestamp;

    @Override
    public void run(int range) throws Exception {
        String sql = "replace into customer(custkey, NAME, gender, address, nationkey, phone, mktsegment, acctbal, times, datime) values(?,?,?,?,?,?,?,?,?,?)";


        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws Exception {
                custkey = getLastId("customer", "custkey") + 1;
                this.ps = connection.prepareStatement(this.sql);
                for (int i = 0; i < range; i++) {
                    makeData(1L);
                    ps.setLong(1, custkey);
                    ps.setString(2, NAME);
                    ps.setString(3, gender);
                    ps.setString(4, address);
                    ps.setInt(5, nationkey);
                    ps.setString(6, phone);
                    ps.setString(7, mktsegment);
                    ps.setBigDecimal(8, acctbal);
                    ps.setLong(9, timestamp);
                    ps.setString(10, datetime);
                    ps.execute();
                    custkey += 1;
                }
            }
        };
    }


    @Override
    public void makeData(Long set) throws Exception {
        String[] Name_sex = {"赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "楮", "卫", "蒋", "沈", "韩", "杨",
                "朱", "秦", "尤", "许", "何", "吕", "施", "张", "孔", "曹", "严", "华", "金", "魏", "陶", "姜",
                "戚", "谢", "邹", "喻", "柏", "水", "窦", "章", "云", "苏", "潘", "葛", "奚", "范", "彭", "郎",
                "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳", "酆", "鲍", "史", "唐",
                "费", "廉", "岑", "薛", "雷", "贺", "倪", "汤", "滕", "殷", "罗", "毕", "郝", "邬", "安", "常",
                "乐", "于", "时", "傅", "皮", "卞", "齐", "康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄",
                "和", "穆", "萧", "尹", "姚", "邵", "湛", "汪", "祁", "毛", "禹", "狄", "米", "贝", "明", "臧",
                "计", "伏", "成", "戴", "谈", "宋", "茅", "庞", "熊", "纪", "舒", "屈", "项", "祝", "董", "梁",
                "杜", "阮", "蓝", "闽", "席", "季", "麻", "强", "贾", "路", "娄", "危", "江", "童", "颜", "郭",
                "梅", "盛", "林", "刁", "锺", "徐", "丘", "骆", "高", "夏", "蔡", "田", "樊", "胡", "凌", "霍",
                "虞", "万", "支", "柯", "昝", "管", "卢", "莫", "经", "房", "裘", "缪", "干", "解", "应", "宗",
                "丁", "宣", "贲", "邓", "郁", "单", "杭", "洪", "包", "诸", "左", "石", "崔", "吉", "钮", "龚",
                "程", "嵇", "邢", "滑", "裴", "陆", "荣", "翁", "荀", "羊", "於", "惠", "甄", "麹", "家", "封",
                "芮", "羿", "储", "靳", "汲", "邴", "糜", "松", "井", "段", "富", "巫", "乌", "焦", "巴", "弓",
                "牧", "隗", "山", "谷", "车", "侯", "宓", "蓬", "全", "郗", "班", "仰", "秋", "仲", "伊", "宫",
                "宁", "仇", "栾", "暴", "甘", "斜", "厉", "戎", "祖", "武", "符", "刘", "景", "詹", "束", "龙",
                "叶", "幸", "司", "韶", "郜", "黎", "蓟", "薄", "印", "宿", "白", "怀", "蒲", "邰", "从", "鄂",
                "索", "咸", "籍", "赖", "卓", "蔺", "屠", "蒙", "池", "乔", "阴", "郁", "胥", "能", "苍", "双",
                "闻", "莘", "党", "翟", "谭", "贡", "劳", "逄", "姬", "申", "扶", "堵", "冉", "宰", "郦", "雍",
                "郤", "璩", "桑", "桂", "濮", "牛", "寿", "通", "边", "扈", "燕", "冀", "郏", "浦", "尚", "农",
                "温", "别", "庄", "晏", "柴", "瞿", "阎", "充", "慕", "连", "茹", "习", "宦", "艾", "鱼", "容",
                "向", "古", "易", "慎", "戈", "廖", "庾", "终", "暨", "居", "衡", "步", "都", "耿", "满", "弘",
                "匡", "国", "文", "寇", "广", "禄", "阙", "东", "欧", "殳", "沃", "利", "蔚", "越", "夔", "隆",
                "师", "巩", "厍", "聂", "晁", "勾", "敖", "融", "冷", "訾", "辛", "阚", "那", "简", "饶", "空",
                "曾", "毋", "沙", "乜", "养", "鞠", "须", "丰", "巢", "关", "蒯", "相", "查", "后", "荆", "红",
                "游", "竺", "权", "逑", "盖", "益", "桓", "公", "晋", "楚", "阎", "法", "汝", "鄢", "涂", "钦",
                "岳", "帅", "缑", "亢", "况", "后", "有", "琴", "商", "牟", "佘", "佴", "伯", "赏", "墨", "哈",
                "谯", "笪", "年", "爱", "阳", "佟"};
        String[] Name_name = {"英", "华", "玉", "秀", "文", "明", "兰", "金", "国", "红", "丽", "小", "梅", "云", "芳", "平", "海", "珍", "荣"};
        String[] Name_name2 = {
                "远", "格", "士", "音", "轻", "目", "条", "呢", "病", "始", "达", "深", "完", "今", "提", "求", "清", "王", "化", "空", "业",
                "思", "切", "怎", "非", "找", "片", "罗", "钱", "紶", "吗", "语", "元", "喜", "曾", "离", "飞", "科", "言", "干", "流", "欢",
                "约", "各", "即", "指", "合", "反", "题", "必", "该", "论", "交", "终", "林", "请", "医", "晚", "制", "球", "决", "窢", "传",
                "画", "保", "读", "运", "及", "则", "房", "早", "院", "量", "苦", "火", "布", "品", "近", "坐", "产", "答", "星", "精", "视",
                "五", "连", "司", "巴"};
        String[] genderArr = {"男", "女"};
        String[] mktsegmentArr = {"三星", "苹果", "华为", "TCL", "小米", "长粒香", "金沙河", "索芙特", "CAREMiLLE", "欧莱雅", "香奈儿"};
        Random rand = new Random();
        int i = rand.nextInt(2);
        if (i % 2 == 0) {
            this.NAME = Name_sex[rand.nextInt(Name_sex.length)] + Name_name[rand.nextInt(Name_name.length)];
        } else
            this.NAME = Name_sex[rand.nextInt(Name_sex.length)] + Name_name[rand.nextInt(Name_name.length)] + Name_name2[rand.nextInt(Name_name2.length)];
        this.gender = genderArr[rand.nextInt(genderArr.length)];
        this.address = "马家屯";
        this.nationkey = rand.nextInt(27) + 1;
        this.phone = HanZiUtil.getTel();
        this.mktsegment = mktsegmentArr[rand.nextInt(mktsegmentArr.length)];
        int flag = rand.nextInt(9) + 1;
        String[] filed = HanZiUtil.getRandomDateTime(flag).split("#");
        this.datetime = filed[0];
        this.acctbal = BigDecimal.valueOf(rand.nextDouble() * 100000);
        this.timestamp = Long.parseLong(filed[1]);

    }
}
