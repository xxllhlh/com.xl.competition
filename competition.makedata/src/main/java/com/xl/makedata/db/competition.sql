/*
 Navicat Premium Data Transfer

 Source Server         : competition
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 192.2.41.12:3306
 Source Schema         : competition

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 12/11/2023 16:06:23
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`
(
    `custkey`    int(11) NULL DEFAULT NULL,
    `NAME`       varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `gender`     varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `address`    varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `nationkey`  int(11) NULL DEFAULT NULL,
    `phone`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `mktsegment` varchar(224) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `acctbal`    decimal(12, 2) NULL DEFAULT NULL,
    `times`      bigint(20) NULL DEFAULT NULL,
    `datime`     varchar(32) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lineitem
-- ----------------------------
DROP TABLE IF EXISTS `lineitem`;
CREATE TABLE `lineitem`
(
    `linekey`       int(11) NULL DEFAULT NULL,
    `orderkey`      int(11) NULL DEFAULT NULL,
    `partkey`       int(11) NULL DEFAULT NULL,
    `suppkey`       int(11) NULL DEFAULT NULL,
    `linenumber`    int(11) NULL DEFAULT NULL,
    `quantity`      double NULL DEFAULT NULL,
    `extendedprice` decimal(10, 2) NULL DEFAULT NULL,
    `discount`      double NULL DEFAULT NULL,
    `tax`           double NULL DEFAULT NULL,
    `returnflag`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `linestatus`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `shipdate`      varchar(32) NULL DEFAULT NULL,
    `commitdate`    varchar(32) NULL DEFAULT NULL,
    `receiptdate`   varchar(32) NULL DEFAULT NULL,
    `shipinstruct`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `shipmode`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `createtime`    varchar(32) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for nation
-- ----------------------------
DROP TABLE IF EXISTS `nation`;
CREATE TABLE `nation`
(
    `nationkey` int(11) NULL DEFAULT NULL,
    `NAME`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `regionkey` int(11) NULL DEFAULT NULL,
    `times`     bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`
(
    `orderkey`      int(11) NULL DEFAULT NULL,
    `custkey`       int(11) NULL DEFAULT NULL,
    `orderstatus`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `totalprice`    decimal(12, 2) NULL DEFAULT NULL,
    `orderdate`     varchar(32) NULL DEFAULT NULL,
    `orderpriority` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `clerk`         int(11) NULL DEFAULT NULL,
    `shippriority`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for part
-- ----------------------------
DROP TABLE IF EXISTS `part`;
CREATE TABLE `part`
(
    `partkey`     int(11) NULL DEFAULT NULL,
    `NAME`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `mfgr`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `brand`       varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `TYPE`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `size`        varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `container`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `retailprice` decimal(12, 2) NULL DEFAULT NULL,
    `times`       bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for partsupp
-- ----------------------------
DROP TABLE IF EXISTS `partsupp`;
CREATE TABLE `partsupp`
(
    `partkey`    int(11) NULL DEFAULT NULL,
    `suppkey`    int(11) NULL DEFAULT NULL,
    `availqty`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `supplycost` decimal(10, 2) NULL DEFAULT NULL,
    `times`      bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`
(
    `regionkey` int(11) NULL DEFAULT NULL,
    `NAME`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`
(
    `suppkey`   int(11) NULL DEFAULT NULL,
    `NAME`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `address`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `nationkey` int(11) NULL DEFAULT NULL,
    `phone`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    `acctbal`   decimal(12, 2) NULL DEFAULT NULL,
    `times`     bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET
FOREIGN_KEY_CHECKS = 1;


