package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import java.util.Random;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PARTSUPP implements Run {
    private int partkey;
    private int suppkey = 1;
    private int availqty;
    private BigDecimal supplycost;
    private long timestamp;

    @Override
    public void run(int range) throws ParseException, SQLException {
        String sql = "replace into partsupp(partkey, suppkey, availqty, supplycost, times) values(?,?,?,?,?)";

        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws Exception {
                partkey = getLastId("partsupp", "partkey") + 1;
                this.ps = connection.prepareStatement(this.sql);
                for (int i = 0; i < range; i++) {
                    makeData(1L);
                        ps.setInt(1, partkey);
                        ps.setInt(2, suppkey);
                        ps.setInt(3, availqty);
                        ps.setBigDecimal(4, supplycost);
                        ps.setLong(5, timestamp);
                        ps.execute();
                        suppkey += 1;
                }
            }
        };
    }

    @Override
    public void makeData(Long set) throws ParseException {
        Random random = new Random();
        this.partkey = random.nextInt(100) + 1;
        this.availqty = random.nextInt(100) + 1;
        this.supplycost = BigDecimal.valueOf(random.nextDouble() * 100);
        int flag = random.nextInt(9) + 1;
        long date = new Date().getTime();
        date -= flag * 100000000L;
        if (flag % 2 == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        }
    }
}
