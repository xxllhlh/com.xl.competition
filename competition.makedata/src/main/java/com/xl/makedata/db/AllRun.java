package com.xl.makedata.db;

import com.xl.makedata.db.bean.*;

import java.sql.Date;
import java.text.SimpleDateFormat;

import static com.xl.makedata.socket.MakeDataToSocket.RANDOM;

/**
 * @author: xl
 * @createTime: 2023/10/6 10:06:23
 * @program: com.xl.competition
 * @description: 任务书一 模拟数据 存入mysql
 */
public class AllRun {
    public static void main(String[] args) throws Exception {
        new REGION().run(1);
        new NATION().run(1);
        new CUSTOMER().run(5000);
        new PART().run(4000);
        new SUPPLIER().run(4000);
        new PARTSUPP().run(4000);
        new Order().run(9000);
        new LineItem().run(10000);
    }
}
