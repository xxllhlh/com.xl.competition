package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class REGION implements Run {
    private int regionkey = 1;
    private String NAME;

    @Override
    public void run(int range) throws Exception {
        String[] regionArr = {"亚洲", "欧洲", "非洲", "大洋洲", "北美洲", "南美洲"};
        String sql = "replace into region(regionkey, NAME) values(?,?)";

        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws SQLException {
                ps = connection.prepareStatement(this.sql);
                while (regionkey < regionArr.length) {
                    NAME = regionArr[regionkey - 1];
                    ps.setInt(1, regionkey);
                    ps.setString(2, NAME);
                    ps.execute();
                    regionkey += 1;
                }
            }
        };
    }

    @Override
    public void makeData(Long set) {

    }
}
