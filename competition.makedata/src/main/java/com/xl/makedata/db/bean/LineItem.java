package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import com.xl.makedata.db.utils.HanZiUtil;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Random;

/**
 * @author: xl
 * @createTime: 2023/11/10 17:40:44
 * @program: com.xl.competition
 * @description: 订单流水明细
 */
public class LineItem implements Run {
    private Integer linekey;
    //订单id
    private Integer orderkey;
    //零件 id
    private Integer partkey;
    //供应商 id
    private Integer suppkey;
    //流水号
    private Integer linenumber;
    //数量
    private Integer quantity;
    //扩展成本，单位成本*数量
    private BigDecimal extendedprice;
    //折扣
    private Double discount;
    //税务
    private Double tax;
    //退回标记
    private String returnflag;
    //流水状态
    private String linestatus;
    //发货日期
    private String shipdate;
    //预计到货日期
    private String commitdate;
    //实际收货日期
    private String receiptdate;
    //运输策略
    private String shipinstruct;
    //运输途径
    private String shipmode;
    private String createTime;

    private static final Random RANDOM = new Random();

    private Integer orderkeylimit;

    private Integer partkeylimit;

    private Integer suppkeylimit;

    @Override
    public void run(int range) throws Exception {
        String sql = "replace into lineitem(linekey,orderkey, partkey, suppkey, linenumber, quantity, extendedprice, discount, tax, returnflag,\n" +
                "                      linestatus, shipdate, commitdate, receiptdate, shipinstruct, shipmode,createTime) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws Exception {
                linekey = getLastId("lineitem", "linekey") + 1;
                orderkeylimit = getLastId("orders", "orderkey");
                partkeylimit = getLastId("part", "partkey");
                suppkeylimit = getLastId("supplier", "suppkey");
                this.ps = connection.prepareStatement(sql);
                for (int i = 0; i < range; i++) {
                    makeData(1L);

                    ps.setInt(1, linekey);
                    ps.setInt(2, orderkey);
                    ps.setInt(3, partkey);
                    ps.setInt(4, suppkey);
                    ps.setInt(5, linenumber);
                    ps.setInt(6, quantity);
                    ps.setBigDecimal(7, extendedprice);
                    ps.setDouble(8, discount);
                    ps.setDouble(9, tax);
                    ps.setString(10, returnflag);
                    ps.setString(11, linestatus);
                    ps.setString(12, shipdate);
                    ps.setString(13, commitdate);
                    ps.setString(14, receiptdate);
                    ps.setString(15, shipinstruct);
                    ps.setString(16, shipmode);
                    ps.setString(17, createTime);
                    ps.execute();
                    linekey += 1;
                }
            }
        };
    }

    @Override
    public void makeData(Long set) throws Exception {
        orderkey = RANDOM.nextInt(orderkeylimit) + 1;
        partkey = RANDOM.nextInt(partkeylimit) + 1;
        suppkey = RANDOM.nextInt(suppkeylimit) + 1;
        linenumber = RANDOM.nextInt(300) + 1;
        quantity = RANDOM.nextInt(1000) + 1;
        extendedprice = BigDecimal.valueOf(RANDOM.nextDouble() * 100000);
        discount = Double.valueOf(String.valueOf(RANDOM.nextDouble()).substring(0, 4));
        tax = Double.valueOf(String.valueOf(RANDOM.nextDouble()).substring(0, 4));
        returnflag = RANDOM.nextInt(100) % 6 == 0 ? "F" : "T";
        linestatus = RANDOM.nextInt(100) % 6 == 0 ? "F" : "T";
        shipdate = HanZiUtil.getRandomDateTime(quantity).split("#")[0];
        commitdate = HanZiUtil.getRandomDateTime(quantity).split("#")[0];
        receiptdate = HanZiUtil.getRandomDateTime(quantity).split("#")[0];
        shipinstruct = HanZiUtil.getRandomHanZiNoSpace(3);
        shipmode = HanZiUtil.getRandomHanZiNoSpace(3);
        createTime =HanZiUtil.getRandomDateTime(quantity).split("#")[0];
    }
}
