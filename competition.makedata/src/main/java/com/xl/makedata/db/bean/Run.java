package com.xl.makedata.db.bean;

interface Run {
    void run(int range) throws Exception;
    void makeData(Long set) throws Exception;
}
