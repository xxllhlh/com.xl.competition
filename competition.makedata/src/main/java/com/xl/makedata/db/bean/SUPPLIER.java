package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import com.xl.makedata.db.utils.HanZiUtil;
import java.util.Random;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

;

public class SUPPLIER implements Run {
    private int suppkey;
    private String name;
    private String address;
    private int nationkey;
    private String phone;
    private BigDecimal acctbal;
    private Long timestamp;

    @Override
    public void run(int range) throws SQLException, ParseException {
        String sql = "replace into supplier(suppkey, NAME, address, nationkey, phone, acctbal, times) values(?,?,?,?,?,?,?)";
        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws Exception {
                suppkey = getLastId("supplier", "suppkey") + 1;
                this.ps = connection.prepareStatement(this.sql);
                for (int i = 0; i < range; i++) {
                    makeData(1L);
                        ps.setInt(1, suppkey);
                        ps.setString(2, name);
                        ps.setString(3, address);
                        ps.setInt(4, nationkey);
                        ps.setString(5, phone);
                        ps.setBigDecimal(6, acctbal);
                        ps.setLong(7, timestamp);
                        ps.execute();
                        suppkey += 1;
                }
            }
        };
    }

    @Override
    public void makeData(Long set) throws ParseException {
        Random random = new Random();
        this.nationkey = random.nextInt(27) + 1;
        this.phone = HanZiUtil.getTel();
        this.address = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(4) + 1);
        this.name = HanZiUtil.getRandomHanZiNoSpace(random.nextInt(5) + 1);
        this.acctbal = BigDecimal.valueOf(random.nextInt(10000) + 1);
        int flag = random.nextInt(10) + 1;
        long date = new Date().getTime();
        date -= flag * 100000000L;
        if (flag % 2 == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        }
    }
}
