package com.xl.makedata.db.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author: xl
 * @createTime: 2023/10/7 20:06:23
 * @program: com.xl.competition
 * @description: 汉字工具类
 */
public class HanZiUtil {
    private static final List<String> manufacturerNames;
    private static final Random random;

    static {
        // 初始化制造商名字列表
        manufacturerNames = new ArrayList<>();
        manufacturerNames.add("华为");
        manufacturerNames.add("小米");
        manufacturerNames.add("OPPO");
        manufacturerNames.add("vivo");
        manufacturerNames.add("联想");
        manufacturerNames.add("魅族");
        manufacturerNames.add("中兴");
        manufacturerNames.add("一加");
        manufacturerNames.add("格力");
        manufacturerNames.add("美的");
        manufacturerNames.add("TCL");
        manufacturerNames.add("海尔");
        manufacturerNames.add("长虹");
        manufacturerNames.add("康佳");

        // 初始化随机数生成器
        random = new Random();
    }
    static String[] Name_sex = {"赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "楮", "卫", "蒋", "沈", "韩", "杨",
            "朱", "秦", "尤", "许", "何", "吕", "施", "张", "孔", "曹", "严", "华", "金", "魏", "陶", "姜",
            "戚", "谢", "邹", "喻", "柏", "水", "窦", "章", "云", "苏", "潘", "葛", "奚", "范", "彭", "郎",
            "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳", "酆", "鲍", "史", "唐",
            "费", "廉", "岑", "薛", "雷", "贺", "倪", "汤", "滕", "殷", "罗", "毕", "郝", "邬", "安", "常",
            "乐", "于", "时", "傅", "皮", "卞", "齐", "康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄",
            "和", "穆", "萧", "尹", "姚", "邵", "湛", "汪", "祁", "毛", "禹", "狄", "米", "贝", "明", "臧",
            "计", "伏", "成", "戴", "谈", "宋", "茅", "庞", "熊", "纪", "舒", "屈", "项", "祝", "董", "梁",
            "杜", "阮", "蓝", "闽", "席", "季", "麻", "强", "贾", "路", "娄", "危", "江", "童", "颜", "郭",
            "梅", "盛", "林", "刁", "锺", "徐", "丘", "骆", "高", "夏", "蔡", "田", "樊", "胡", "凌", "霍",
            "虞", "万", "支", "柯", "昝", "管", "卢", "莫", "经", "房", "裘", "缪", "干", "解", "应", "宗",
            "丁", "宣", "贲", "邓", "郁", "单", "杭", "洪", "包", "诸", "左", "石", "崔", "吉", "钮", "龚",
            "程", "嵇", "邢", "滑", "裴", "陆", "荣", "翁", "荀", "羊", "於", "惠", "甄", "麹", "家", "封",
            "芮", "羿", "储", "靳", "汲", "邴", "糜", "松", "井", "段", "富", "巫", "乌", "焦", "巴", "弓",
            "牧", "隗", "山", "谷", "车", "侯", "宓", "蓬", "全", "郗", "班", "仰", "秋", "仲", "伊", "宫",
            "宁", "仇", "栾", "暴", "甘", "斜", "厉", "戎", "祖", "武", "符", "刘", "景", "詹", "束", "龙",
            "叶", "幸", "司", "韶", "郜", "黎", "蓟", "薄", "印", "宿", "白", "怀", "蒲", "邰", "从", "鄂",
            "索", "咸", "籍", "赖", "卓", "蔺", "屠", "蒙", "池", "乔", "阴", "郁", "胥", "能", "苍", "双",
            "闻", "莘", "党", "翟", "谭", "贡", "劳", "逄", "姬", "申", "扶", "堵", "冉", "宰", "郦", "雍",
            "郤", "璩", "桑", "桂", "濮", "牛", "寿", "通", "边", "扈", "燕", "冀", "郏", "浦", "尚", "农",
            "温", "别", "庄", "晏", "柴", "瞿", "阎", "充", "慕", "连", "茹", "习", "宦", "艾", "鱼", "容",
            "向", "古", "易", "慎", "戈", "廖", "庾", "终", "暨", "居", "衡", "步", "都", "耿", "满", "弘",
            "匡", "国", "文", "寇", "广", "禄", "阙", "东", "欧", "殳", "沃", "利", "蔚", "越", "夔", "隆",
            "师", "巩", "厍", "聂", "晁", "勾", "敖", "融", "冷", "訾", "辛", "阚", "那", "简", "饶", "空",
            "曾", "毋", "沙", "乜", "养", "鞠", "须", "丰", "巢", "关", "蒯", "相", "查", "后", "荆", "红",
            "游", "竺", "权", "逑", "盖", "益", "桓", "公", "晋", "楚", "阎", "法", "汝", "鄢", "涂", "钦",
            "岳", "帅", "缑", "亢", "况", "后", "有", "琴", "商", "牟", "佘", "佴", "伯", "赏", "墨", "哈",
            "谯", "笪", "年", "爱", "阳", "佟"};
    static String[] Name_name = {"英", "华", "玉", "秀", "文", "明", "兰", "金", "国", "红", "丽", "小", "梅", "云", "芳", "平", "海", "珍", "荣"};
    static String[] Name_name2 = {
            "远", "格", "士", "音", "轻", "目", "条", "呢", "病", "始", "达", "深", "完", "今", "提", "求", "清", "王", "化", "空", "业",
            "思", "切", "怎", "非", "找", "片", "罗", "钱", "紶", "吗", "语", "元", "喜", "曾", "离", "飞", "科", "言", "干", "流", "欢",
            "约", "各", "即", "指", "合", "反", "题", "必", "该", "论", "交", "终", "林", "请", "医", "晚", "制", "球", "决", "窢", "传",
            "画", "保", "读", "运", "及", "则", "房", "早", "院", "量", "苦", "火", "布", "品", "近", "坐", "产", "答", "星", "精", "视",
            "五", "连", "司", "巴"};
    String[] genderArr = {"男", "女"};

    public static String getRandomDateTime(int flag) {
        long date = new java.util.Date().getTime() - (random.nextInt(400000000) + 1L);
        if (flag % (random.nextInt(3) + 1) == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return dateFormat.format(date) + "#" + date;
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date) + "#" + date;
        }
    }

    public static BigDecimal getRandomRedPacketBetweenMinAndMax(BigDecimal min, BigDecimal max){
        float minF = min.floatValue();
        float maxF = max.floatValue();


        //生成随机数
        BigDecimal db = new BigDecimal(Math.random() * (maxF - minF) + minF);

        //返回保留两位小数的随机数。不进行四舍五入
        return db.setScale(2, RoundingMode.DOWN);
    }

    public static String getUserName(){
        int i = random.nextInt(100) + 1;
        if (i % 3 == 0) {
            return  Name_sex[random.nextInt(Name_sex.length)] + Name_name[random.nextInt(Name_name.length)];
        } else
            return  Name_sex[random.nextInt(Name_sex.length)] + Name_name[random.nextInt(Name_name.length)] + Name_name2[random.nextInt(Name_name2.length)];
    }

    public static String getRandomManufacturerName() {
        // 从列表中随机选择一个制造商名字
        int randomIndex = random.nextInt(manufacturerNames.size());
        return manufacturerNames.get(randomIndex);
    }

    private static final String[] telFirst = "134,135,136,137,138,139,150,151,152,157,158,159,130,131,132,155,156,133,153".split(",");

    public static String getTel() {
        int index = getNum(0, telFirst.length - 1);
        String first = telFirst[index];
        String second = String.valueOf(getNum(1, 888) + 10000).substring(1);
        String third = String.valueOf(getNum(1, 9100) + 10000).substring(1);
        return first + second + third;
    }

    public static int getNum(int start, int end) {
        return (int) (Math.random() * (end - start + 1) + start);
    }


    /**
     * 返回一个汉字
     *
     * @param
     * @return
     */
    public static char getRandomHanZi() {
        return (char) (0x4e00 + (int) (Math.random() * (0x9fa5 - 0x4e00 + 1)));
    }

    /**
     * 获取多个汉字中间有空格
     *
     * @param size 汉字个数
     * @return
     */
    public static String getRandomHanZi(int size) {

        if (size <= 0) {
            size = 1;
        }
        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            stringBuffer.append(getRandomHanZi() + " ");
        }
        return stringBuffer.toString();
    }

    /**
     * 获取多个汉字中间无空格
     *
     * @param size 汉字个数
     * @return
     */
    public static String getRandomHanZiNoSpace(int size) {

        if (size <= 0) {
            size = 1;
        }
        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            stringBuffer.append(getRandomHanZi());
        }
        return stringBuffer.toString();
    }
}