package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class NATION implements Run {
    public int getNationkey() {
        return nationkey;
    }

    private int nationkey;
    private String NAME;
    private int regionkey;
    private long timestamp;
    public String[] nationArr = {
            "中国", "蒙古", "朝鲜", "韩国", "日本", "越南",
            "英国", "法国", "冰岛", "丹麦", "挪威", "瑞典", "芬兰",
            "埃及", "利比亚", "突尼斯", "阿尔及利亚", "摩洛哥", "尼日尔",
            "澳大利亚", "新西兰",
            "美国", "加拿大",
            "哥伦比亚", "委内瑞拉", "圭亚那", "苏里南"
    };

    @Override
    public void run(int range) throws Exception {
        this.nationkey = 1;
        String sql = "replace into nation(nationkey, NAME, regionkey, times) values(?,?,?,?)";


        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws SQLException, ParseException {
                this.ps = connection.prepareStatement(this.sql);
                for (int i = 0; i < nationArr.length; i++) {
                    makeData(1L);
                    ps.setLong(1, nationkey);
                    ps.setString(2, NAME);
                    ps.setInt(3, regionkey);
                    ps.setLong(4, timestamp);
                    ps.execute();
                    nationkey += 1;
                }
            }

        };
    }

    @Override
    public void makeData(Long set) throws ParseException {
        Random random = new Random();


        String[] A = {"中国", "蒙古", "朝鲜", "韩国", "日本", "越南"};
        String[] O = {"英国", "法国", "冰岛", "丹麦", "挪威", "瑞典", "芬兰"};
        String[] F = {"埃及", "利比亚", "突尼斯", "阿尔及利亚", "摩洛哥", "尼日尔"};
        String[] D = {"澳大利亚", "新西兰"};
        String[] B = {"美国", "加拿大"};
        String[] N = {"哥伦比亚", "委内瑞拉", "圭亚那", "苏里南"};
        ArrayList<String> strings = new ArrayList<>();

        String[] regionArr = {"亚洲", "欧洲", "非洲", "大洋洲", "北美洲", "南美洲"};

        this.NAME = nationArr[this.nationkey - 1];
        if (Arrays.toString(A).contains(this.NAME)) {
            this.regionkey = 1;
        } else if (Arrays.toString(O).contains(this.NAME)) {
            this.regionkey = 2;
        } else if (Arrays.toString(F).contains(this.NAME)) {
            this.regionkey = 3;
        } else if (Arrays.toString(D).contains(this.NAME)) {
            this.regionkey = 4;
        } else if (Arrays.toString(B).contains(this.NAME)) {
            this.regionkey = 5;
        } else
            this.regionkey = 6;
        int flag = random.nextInt(9) + 1;
        long date = new Date().getTime();
        date -= flag * 100000000;
        if (flag % 2 == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String format = dateFormat.format(date);
            this.timestamp = dateFormat.parse(format).getTime();
        }
    }
}
