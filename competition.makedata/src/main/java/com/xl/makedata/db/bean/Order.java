package com.xl.makedata.db.bean;

import com.xl.makedata.db.utils.BaseJdbcSink;
import com.xl.makedata.db.utils.HanZiUtil;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Date;
import java.util.Random;

import static com.xl.makedata.socket.MakeDataToSocket.RANDOM;

/**
 * @author: xl
 * @createTime: 2023/11/10 17:28:42
 * @program: com.xl.competition
 * @description: 订单表
 */
public class Order implements Run {

    private static final Random RANDOM = new Random();

    private Integer orderkey;
    //customer id
    private Integer custkey;
    //订单状态
    private String orderstatus;
    //订单总额
    private BigDecimal totalprice;
    //下单日期
    private String orderdate;
    //订单优先级
    private String orderpriority;
    //记录订单来自客户的key
    private Integer clerk;
    //发货优先级
    private String shippriority;

    private Integer custkeyLimit;

    @Override
    public void run(int range) throws Exception {
        String sql = "replace into orders(orderkey, custkey, orderstatus, totalprice, orderdate, orderpriority, clerk, shippriority) values(?,?,?,?,?,?,?,?)";
        new BaseJdbcSink(sql) {
            @Override
            public void Write2Mysql() throws Exception {
                orderkey = getLastId("orders", "orderkey") + 1;
                custkeyLimit = getLastId("customer", "custkey");
                this.ps = connection.prepareStatement(sql);
                for (int i = 1; i <= range; i++) {
                    makeData(1L);
                    ps.setInt(1, orderkey);
                    ps.setInt(2, custkey);
                    ps.setString(3, orderstatus);
                    ps.setBigDecimal(4, totalprice);
                    ps.setString(5, orderdate);
                    ps.setString(6, orderpriority);
                    ps.setInt(7, clerk);
                    ps.setString(8, shippriority);
                    orderkey += 1;
                    ps.execute();
                }

            }
        };
    }

    @Override
    public void makeData(Long set) throws Exception {
        custkey = RANDOM.nextInt(custkeyLimit) + 1;
        orderstatus = (RANDOM.nextInt(100) + 1) % 6 == 0 ? "F" : "T";
        totalprice = BigDecimal.valueOf(RANDOM.nextDouble() * 100000);
        orderdate = HanZiUtil.getRandomDateTime(orderkey).split("#")[0];
        orderpriority = String.valueOf(RANDOM.nextInt(100) + 1);
        clerk = RANDOM.nextInt(custkeyLimit) + 1;
        shippriority = String.valueOf(RANDOM.nextInt(100) + 1);
    }
}
