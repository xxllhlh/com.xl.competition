package com.xl.competition.module_c.old.bean

import com.alibaba.fastjson.JSON

import java.math.BigDecimal
import java.util.Date

case class OrderInfo(
                      var id: Long = 0L,
                      var consignee: String = null,
                      var consigneeTel: String = null,
                      var totalAmount: BigDecimal = null,
                      var orderStatus: String = null,
                      var userId: Long = 0L,
                      var deliveryAddress: String = null,
                      var orderComment: String = null,
                      var outTradeNo: String = null,
                      var tradeBody: String = null,
                      var createTime: Date = null,
                      var operateTime: Date = null,
                      var expireTime: Date = null,
                      var trackingNo: String = null,
                      var parentOrderId:Long = 0L,
                      var imgUrl: String = null,
                      var provinceId: Integer = null,
                      var originalTotalAmount: BigDecimal = null,
                      var feightFee: BigDecimal = null,
                      var activityReduceAmount: BigDecimal = null,
                      var couponReduceAmount: BigDecimal = null
                    ){
}

