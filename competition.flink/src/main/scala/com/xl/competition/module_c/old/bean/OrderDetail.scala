package com.xl.competition.module_c.old.bean

import java.math.BigDecimal
import java.util.Date

case class OrderDetail(
                        var id: Long = 0L,
                        var orderId: Long = 0L,
                        var skuId: Long = 0L,
                        var skuName: String = null,
                        var imgUrl: String = null,
                        var orderPrice: BigDecimal = null,
                        var skuNum: Long = 0L,
                        var createTime: Date = null,
                        var operateTime: Date = null,
                        var splitTotalAmount: BigDecimal = null,
                        var splitCouponAmount: BigDecimal = null,
                        var splitActivityAmount: BigDecimal = null,
                        var sourceType: String = null,
                        var sourceId: Long = 0L
                      )
