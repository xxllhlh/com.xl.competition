package com.xl.competition_web.controller;

import com.xl.competition_web.R;
import com.xl.competition_web.server.CoreService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author: xl
 * @createTime: 2023/12/15 19:24:50
 * @program: com.xl.competition
 * @description: 数据可视化接口
 */
@RestController
@RequestMapping("/get")
public class CoreController {
    @Resource
    CoreService coreService;

    @GetMapping("/high-amount-province")
    public R getHighAmountProvince() {
        return coreService.getAmountHighProvince();
    }

    @GetMapping("/low-amount-province")
    public R getLowAmountProvince() {
        return coreService.getAmountLowProvince();
    }

    @PostMapping("/avg-expense-province")
    public R getAvgExpenseProvince(@RequestBody Map<String,String> param ){
        return coreService.getAvgExpenseProvince(param);
    }
}
