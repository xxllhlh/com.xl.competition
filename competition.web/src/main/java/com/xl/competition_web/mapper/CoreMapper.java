package com.xl.competition_web.mapper;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author: xl
 * @createTime: 2023/12/15 19:30:54
 * @program: com.xl.competition
 * @description:
 */
@Mapper
public interface CoreMapper {
    @MapKey("")
    List<Map<String, Object>> getAmountHighProvince();
    @MapKey("")
    List<Map<String, Object>> getAmountLowProvince();
    @MapKey("")
    List<Map<String, Object>> getAvgExpenseProvince(Map<String, String> param);
}
