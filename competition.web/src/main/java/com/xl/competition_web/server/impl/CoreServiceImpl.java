package com.xl.competition_web.server.impl;

import com.xl.competition_web.R;
import com.xl.competition_web.mapper.CoreMapper;
import com.xl.competition_web.server.CoreService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author: xl
 * @createTime: 2023/12/15 19:39:40
 * @program: com.xl.competition
 * @description:
 */
@Service
public class CoreServiceImpl implements CoreService {
    @Resource
    CoreMapper coreMapper;

    @Override
    public R getAmountHighProvince() {
        List amountHighProvince = coreMapper.getAmountHighProvince();
        return R.ok().put("data", amountHighProvince);
    }

    @Override
    public R getAmountLowProvince() {
        List amountLowProvince = coreMapper.getAmountLowProvince();
        return R.ok().put("data", amountLowProvince);
    }

    @Override
    public R getAvgExpenseProvince(Map<String,String> param) {
        List avgExpenseProvince = coreMapper.getAvgExpenseProvince(param);
        return R.ok().put("data", avgExpenseProvince);
    }
}
