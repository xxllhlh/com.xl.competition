package com.xl.competition_web.server;

import com.xl.competition_web.R;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: xl
 * @createTime: 2023/12/15 19:30:37
 * @program: com.xl.competition
 * @description:
 */
@Service
public interface CoreService {

    R getAmountHighProvince();
    R getAmountLowProvince();

    R getAvgExpenseProvince(Map<String,String> param);
}
