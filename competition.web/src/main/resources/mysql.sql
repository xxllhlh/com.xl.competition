show databases;
use gmall;

show tables;

select *
from base_province;

select *
from order_info;


select oi.province_id,
       bp.name              province_name,
       sum(oi.total_amount) total_amount
from order_info oi
         join base_province bp
              on oi.province_id = bp.id
where year(oi.create_time) = '2023' and bp.name !='台湾'
group by province_id, name
order by total_amount desc
limit 5
;

select oi.province_id,
       bp.name,
       avg(oi.total_amount) total_amonut
from order_info oi
         join base_province bp
              on bp.id = oi.province_id
where create_time > '2023-01-01 00:00:00' and create_time < '2023-12-31 23:59:59'
group by bp.id, bp.name
order by total_amonut desc
limit 5